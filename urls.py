import aldryn_addons.urls
from aldryn_django.utils import i18n_patterns
from django.conf import settings
from django.urls import include, path
from django.views.generic import RedirectView
from djangocms_helpers.sentry_500_error_handler.views import collect_500_error_user_feedback_view

urlpatterns = [
    path("", RedirectView.as_view(pattern_name="polling:homepage")),
] + aldryn_addons.urls.patterns() + i18n_patterns(
    path("", RedirectView.as_view(pattern_name="polling:homepage")),
    path("reports/", include("backend.covid_report.polling.urls")),
    path("accounts/", include("allauth.urls")),
    path("accounts/", include("backend.auth.urls", namespace="backend_auth")),
    path("hijack/", include('hijack.urls', namespace='hijack')),
    *aldryn_addons.urls.i18n_patterns(),  # MUST be the last entry!
)


if not settings.DEBUG:
    handler500 = collect_500_error_user_feedback_view
    handler404 = 'backend.http_errors.views.handler404'
