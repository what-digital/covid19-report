import os
import logging
import warnings
import secrets
from typing import List
from enum import Enum
from datetime import timedelta

import sentry_sdk
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from sentry_sdk.integrations.celery import CeleryIntegration
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.integrations.logging import LoggingIntegration
from env_settings import env
from dotenv import load_dotenv, find_dotenv


load_dotenv(find_dotenv(".env-local"))


################################################################################
# === django packages === #
################################################################################


INSTALLED_ADDONS = [
    # <INSTALLED_ADDONS>  # Warning: text inside the INSTALLED_ADDONS tags is auto-generated. Manual changes will be overwritten.
    "aldryn-addons",
    "aldryn-django",
    "aldryn-sso",
    "aldryn-celery",
    # </INSTALLED_ADDONS>
]


ALDRYN_SSO_ALWAYS_REQUIRE_LOGIN = False


# for native setup must be before `aldryn_addons.settings.load()`
IS_NATIVE_SETUP = env.get_bool("IS_NATIVE_SETUP", default=False)
if IS_NATIVE_SETUP:
    DATABASE_URL = env.get(
        "DATABASE_URL", "postgres://postgres@localhost:5432/db"
    )


import aldryn_addons.settings

aldryn_addons.settings.load(locals())


################################################################################
# === django packages === #
################################################################################


INSTALLED_APPS: List[str] = locals()["INSTALLED_APPS"]
MIDDLEWARE: List[str] = locals()["MIDDLEWARE"]
BASE_DIR: str = locals()["BASE_DIR"]
STATIC_URL: str = locals()["STATIC_URL"]
HTTP_PROTOCOL: str = locals()["STATIC_URL"]
TEMPLATES: List[dict] = locals()["TEMPLATES"]

USE_TZ = True
TIME_ZONE = "Europe/Zurich"

DATE_FORMAT = "F j, Y"


class DivioEnv(Enum):
    LOCAL = "local"
    TEST = "test"
    LIVE = "live"


DIVIO_ENV_ENUM = DivioEnv
DIVIO_ENV = DivioEnv(env.get("STAGE", "local"))


LANGUAGE_CODE = 'de'


if DIVIO_ENV == DIVIO_ENV_ENUM.LIVE:
    LANGUAGES = [
        ('de', 'German'),
    ]
else:
    LANGUAGES = [
        ('de', 'German'),
        ('en', 'English'),
    ]


INSTALLED_APPS.insert(
    0, "backend.auth"
)  # for USERNAME_FIELD = 'email', before `cms` since it has a User model

INSTALLED_APPS.extend(
    [
        # django packages
        "allauth",
        "allauth.account",
        "allauth.socialaccount",
        "cuser",  # for USERNAME_FIELD = 'email' in backend.auth
        "invitations",
        "gtm",
        "solo",
        "rest_framework",
        "import_export",
        "adminsortable2",
        "admin_reorder",
        "django_extensions",
        "django_countries",
        "logentry_admin",
        "adminutils",
        "django_object_actions",
        "sekizai",
        "widget_tweaks",
        "djangocms_helpers",
        "djangocms_helpers.sentry_500_error_handler",
        "hijack_admin",

        # project
        "backend.covid_report.polling",
        "backend.site_config",
    ]
)


MIDDLEWARE.extend(
    [
        # django packages
        "admin_reorder.middleware.ModelAdminReorder",
        
        # backend.polling
        "backend.covid_report.polling.middleware.RedirectExceptionMiddleware",
    ]
)


AUTH_PASSWORD_VALIDATORS = [
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
]


AUTH_USER_MODEL = "backend_auth.User"


STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "frontend/"),
]

default_template_engine: dict = TEMPLATES[0]
default_template_engine["DIRS"].extend(
    [os.path.join(BASE_DIR, "backend/templates/"), ]
)
default_template_engine["OPTIONS"]["context_processors"].extend(
    [
        "django_settings_export.settings_export",
        "sekizai.context_processors.sekizai",
    ]
)


if DIVIO_ENV == DivioEnv.LOCAL:
    email_backend_default = "django.core.mail.backends.console.EmailBackend"
else:
    email_backend_default = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_BACKEND = env.get("EMAIL_BACKEND", default=email_backend_default)

DEFAULT_FROM_EMAIL = "KFS Basel-Landschaft <info@coreport.ch>"
DEFAULT_REPLY_TO_EMAIL = "dashboard.kfs@bl.ch"


################################################################################
# === django packages === #
################################################################################


# allauth
AUTHENTICATION_BACKENDS = [
    # Needed to login by username in Django admin, regardless of `allauth`
    "django.contrib.auth.backends.ModelBackend",
    "allauth.account.auth_backends.AuthenticationBackend",
]
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_VERIFICATION = "none"
ACCOUNT_DEFAULT_HTTP_PROTOCOL = HTTP_PROTOCOL
ACCOUNT_AUTHENTICATION_METHOD = "email"
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_USER_MODEL_USERNAME_FIELD = None
ACCOUNT_SIGNUP_PASSWORD_ENTER_TWICE = False
ACCOUNT_AUTHENTICATED_LOGIN_REDIRECTS = (
    False  # otherwise admins can't access the login view
)
ACCOUNT_LOGIN_ON_PASSWORD_RESET = True
LOGIN_REDIRECT_URL = "/"
LOGIN_REDIRECT = "/"
LOGIN_URL = reverse_lazy("account_login")
CONFIRM_EMAIL_ON_GET = True
ACCOUNT_SIGNUP_FORM_CLASS = "backend.auth.forms.CovidSignupForm"


GTM_CONTAINER_ID = env.get("GTM_CONTAINER_ID", "GTM-1234")


WEBPACK_DEV_URL = env.get(
    "WEBPACK_DEV_URL", default="http://localhost:8090/assets/"
)


SETTINGS_EXPORT = [
    "DEBUG",
    "WEBPACK_DEV_URL",
    "DIVIO_ENV",
    "DIVIO_ENV_ENUM",
    "SENTRY_IS_ENABLED",
    "META_SITE_PROTOCOL",
    "SENTRY_DSN",
    "DEFAULT_FROM_EMAIL",
    "DEFAULT_REPLY_TO_EMAIL",
]


SENTRY_IS_ENABLED = env.get_bool("SENTRY_IS_ENABLED", False)
SENTRY_DSN = env.get(
    "SENTRY_DSN", "https://e7cb2d13c02144f5adc989f8e46dd84d@sentry.io/4929845"
)
if SENTRY_IS_ENABLED:
    # noinspection PyTypeChecker
    sentry_sdk.init(
        dsn=SENTRY_DSN,
        integrations=[
            DjangoIntegration(),
            LoggingIntegration(
                level=logging.INFO,  # Capture info and above as breadcrumbs
                event_level=None,  # Send no events from log messages
            ),
            CeleryIntegration(),
        ],
        environment=DIVIO_ENV.value,
        send_default_pii=True,
    )


ADMIN_REORDER = [
    {
        "label": "Users",
        "app": "backend_auth",
        "models": [
            "backend_auth.User",
            "backend_auth.Invitation",
            "backend_auth.Organization",
            "backend_auth.OrganizationType",
        ],
    },
    {
        "label": "COVID-19 Polling",
        "app": "polling",
        "models": [
            "polling.Metric",
            "polling.Datapoint",
            {
                'model': 'polling.Timeslot',
                'label': _('Answers deadlines'),
            },
            {
                'model': 'polling.PollingFrequency',
                'label': _('Reporting frequencies'),
            },
        ],
    },
    {
        "label": "Administration",
        "app": "admin",
        "models": [
            'site_config.SiteConfig',
            "admin.LogEntry",
            "auth.Group",
        ],
    },
]


META_SITE_PROTOCOL = "http" if DIVIO_ENV == DivioEnv.LOCAL else "https"
META_USE_SITES = True


HIJACK_REGISTER_ADMIN = False
HIJACK_ALLOW_GET_REQUESTS = True

# Celery settings

locals().pop("CELERYBEAT_SCHEDULER", None)  # Do not use the database scheduler


CELERYBEAT_SCHEDULE = {
    "generate_slots": {
        "task": "backend.covid_report.polling.tasks.regenerate_timeslots_and_datapoints",  # NOQA
        "schedule": timedelta(minutes=10),
    },
    "copy_last_value_if_slot_was_missed": {
        "task": "backend.covid_report.polling.tasks.copy_last_value_if_slot_was_missed",  # NOQA
        "schedule": timedelta(minutes=5),
    },
}

# COVID-19 settings

# Period in the future to generate timeslots for
TIMESLOTS_GENERATION_PERIOD = timedelta(days=7)

EXPORT_USER_PASSWORD = env.get("EXPORT_USER_PASSWORD")
if not EXPORT_USER_PASSWORD:
    EXPORT_USER_PASSWORD = secrets.token_urlsafe(32)
    warnings.warn(
        f"EXPORT_USER_PASSWORD environment variable not set, "
        f"autogenerating password: {EXPORT_USER_PASSWORD}"
    )

BASICAUTH_USERS = {
    "export": EXPORT_USER_PASSWORD,
}
BASICAUTH_REALM = "CoReport Export"
