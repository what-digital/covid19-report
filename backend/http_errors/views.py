from django.http import HttpRequest
from django.http import HttpResponse
from django.template import RequestContext
from django.template.loader import get_template
from sekizai.context_processors import sekizai


def handler404(
    request: HttpRequest,
    exception: Exception,
    template_name: str = "404.html",
) -> HttpResponse:
    template = get_template(template_name)
    context = RequestContext(request)
    context.update(sekizai())
    return HttpResponse(template.render(context.flatten()))
