from datetime import datetime
from django.db.models import QuerySet
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.viewsets import ModelViewSet
from backend.auth.mixins import OrganizationRequiredMixin
from backend.auth.mixins import ReporterRequiredMixin
from backend.covid_report.polling.api.serializers import DatapointSerializer, DatapointsSerializer
from backend.covid_report.polling.models import Datapoint


class DatapointUpdateApiView(
    RetrieveUpdateAPIView,
    ReporterRequiredMixin,
    OrganizationRequiredMixin,
):
    model = Datapoint
    serializer_class = DatapointSerializer

    def get_queryset(self) -> QuerySet:
        return Datapoint.objects.filter(
            organization__in=self.request.user.get_accessible_organizations(),
        )



class DatapointsViewSet(ModelViewSet,ReporterRequiredMixin,OrganizationRequiredMixin):
    """
    Endpoint to pull list of Answers. Query is limited to organization accessible by user.

    Parameters:

        organization (ex: abc-wehr): filter by slug of an organization

        timeslot (ex: 05-02-2022): from when to build report

        question (ex: how%20is%20it%3F): url encoded question 

    """
    model = Datapoint
    serializer_class = DatapointsSerializer

    def get_queryset(self) -> QuerySet:
        filters = {
            'organization__in':self.request.user.get_accessible_organizations()
        }

        if 'organization' in self.request.GET:
            filters['organization__slug'] = self.request.GET['organization']

        if 'timeslot' in self.request.GET:
            filters['timeslot__deadline__gte'] = datetime.strptime(self.request.GET['timeslot'], '%d-%m-%Y')

        if 'question' in self.request.GET:
            filters['timeslot__metric__question'] =  self.request.GET['question']

        return Datapoint.objects.filter(**filters)

