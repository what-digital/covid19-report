from typing import Union

from django.utils import timezone
from django.utils.translation import pgettext
from rest_framework import serializers

from backend.covid_report.polling.models import Datapoint, Timeslot
from backend.covid_report.polling.models import Status


DatapointValue = Union[int, str, Status]


class DatapointValueField(serializers.Field):
    parent: 'DatapointSerializer'

    def to_representation(self, value: DatapointValue) -> Union[str, int]:
        instance = self.parent.instance
        if instance.is_numeric():
            return instance.numeric_value
        elif instance.is_text():
            return instance.text_value
        elif instance.is_status():
            return instance.status_value.value
        else:
            raise ValueError("You need to add support for this type.")

    def to_internal_value(self, data: Union[str, int]) -> DatapointValue:
        value = self._clean_value(data)
        if self.parent.instance.is_numeric() or self.parent.instance.is_text():
            return value
        elif self.parent.instance.is_status():
            return Status(value)

    def _clean_value(self, value: Union[str, int]):
        if self.parent.instance.is_numeric():
            type_error = pgettext("fm", "Must be a number")
            if type(value) is str:
                try:
                    value = int(value)
                except ValueError:
                    raise serializers.ValidationError(type_error)
            elif type(value) is not int:
                raise serializers.ValidationError(type_error)
            if value < 0:
                raise serializers.ValidationError(
                    pgettext("fm", "The value must be higher or equal to 0"),
                )

            return int(value)
        elif self.parent.instance.is_text():
            if not value:
                self.fail('required')
            return value
        elif self.parent.instance.is_status():
            try:
                Status(int(value))
            except ValueError:
                raise serializers.ValidationError(
                    pgettext("fm", "Invalid status")
                )
            return int(value)
        else:
            raise ValueError("You need to add support for this type.")


class DatapointSerializer(serializers.ModelSerializer):
    value = DatapointValueField(required=True)

    instance: Datapoint

    def save(self, **kwargs) -> Datapoint:
        self._set_value_on_instance()
        
        self.instance.submitting_user = self.context['request'].user
        self.instance.submission_datetime = timezone.now()
        
        return super().save(**kwargs)

    def _set_value_on_instance(self):
        value: DatapointValue = self._get_value_and_prevent_drf_setting_it_on_model()
        instance = self.instance
        if instance.is_numeric():
            instance.numeric_value = value
        elif instance.is_text():
            instance.text_value = value
        elif instance.is_status():
            instance.status_value = value
        else:
            raise ValueError("You need to add support for this type.")

    def _get_value_and_prevent_drf_setting_it_on_model(self) -> DatapointValue:
        return self.validated_data.pop('value')

    class Meta:
        model = Datapoint
        fields = [
            'value',
            'comment',
        ]



class TimeslotSerializer(serializers.ModelSerializer):
    deadline = serializers.DateTimeField(format='%d-%m-%Y')
    metric = serializers.CharField(max_length=200)
    class Meta:
        model = Timeslot
        fields = ['id','metric', 'metric_id','deadline']

class DatapointsSerializer(serializers.ModelSerializer):
    timeslot = TimeslotSerializer()
    organization = serializers.SlugRelatedField(
        read_only=True,
        slug_field='slug'
    )    
    class Meta:
        model = Datapoint
        fields = ['id','timeslot', 'organization']
