from datetime import datetime
from datetime import timedelta
from typing import Union
from unittest import skip

from django.contrib.auth import get_user_model
from django.db import connection
from django.db.models import QuerySet
from django.test.utils import CaptureQueriesContext
from django.utils import timezone

from backend.auth.models import Organization
from backend.auth.models import OrganizationType
from backend.covid_report.polling.models import Datapoint
from backend.covid_report.polling.models import Metric
from backend.covid_report.polling.models import MetricType
from backend.covid_report.polling.models import PollingFrequency
from backend.covid_report.polling.models import Status
from backend.covid_report.polling.tests.cases import CovidTestCase
from backend.covid_report.polling.utils import copy_last_value_if_slot_was_missed
from backend.site_config.models import SiteConfig

User = get_user_model()


class CopyLastValueTest(CovidTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.org_1.is_automatically_copy_last_answer = True
        cls.org_1.save()

        cls.site_config = SiteConfig.get_solo()
        cls.site_config.system_user = User.objects.create(
            email="vladimir@what.digital",
            is_can_report=True,
        )
        cls.site_config.save()

        cls.org_test_type = OrganizationType.objects.create(name="Test")

        cls.org_3 = Organization.objects.create(name="Test Organisation")
        cls.org_3.types.add(cls.org_type)
        cls.org_3.is_automatically_copy_last_answer = True
        cls.org_3.save()

        cls.user.organizations.add(cls.org_3)

        cls.since = timezone.now() - timedelta(days=3)
        cls.until = cls.since + timedelta(days=3)

    def test_no_overdue_values(self):
        value = "Text value"
        self._generate_metrics(self.since, self.until, MetricType.TEXT.value)
        self._fill_values(self.since, self.since + timedelta(days=3), value=value)

        self._check_no_overdue_values(self.since, self.until, value)

        copy_last_value_if_slot_was_missed()

        self._check_no_overdue_values(self.since, self.until, value)

    def _check_no_overdue_values(self, since: datetime, until: datetime, value: str) -> None:
        self.assertEqual(6, Datapoint.objects.before(until).filter(organization=self.org_1).count())

        for datapoint in Datapoint.objects.before(until).filter(organization=self.org_1).iterator():
            self.assertEqual(self.user, datapoint.submitting_user)
            self.assertEqual(value, datapoint.text_value)

        self._check_empty_datapoints(
            Datapoint.objects.after(since + timedelta(days=3)).filter(organization=self.org_1)
        )

    def test_copy_last_text_value_if_slot_was_missed(self):
        value = "Text value"
        self._generate_metrics(self.since, self.until, MetricType.TEXT.value)
        self._fill_values(self.since, self.since + timedelta(days=2), value=value)
        datapoints = self._filter_filled_datapoints_by_user()

        self.assertEqual(4, datapoints.count())

        self._check_filled_datapoints_by_user(datapoints, value)

        self._check_empty_datapoints(
            Datapoint.objects.after(self.since + timedelta(days=2)).filter(organization=self.org_1)
        )

        with CaptureQueriesContext(connection) as context:
            copy_last_value_if_slot_was_missed()
            self.assertLessEqual(len(context.captured_queries), 15)

        self.assertEqual(4, datapoints.count())

        self._check_filled_datapoints_by_user(datapoints, value)

        autofilled_datapoints = (
            Datapoint.objects
                .after(self.since + timedelta(days=2))
                .before(self.since + timedelta(days=3))
                .filter(organization=self.org_1)
        )
        self.assertEqual(2, autofilled_datapoints.count())

        for datapoint in autofilled_datapoints:
            self.assertEqual(self.site_config.system_user, datapoint.submitting_user)
            self.assertEqual(value, datapoint.text_value)

        self._check_empty_datapoints(
            Datapoint.objects.after(self.since + timedelta(days=3)).filter(organization=self.org_1)
        )

    def test_copy_numeric_value_if_slot_was_missed(self):
        value = 123

        self._generate_metrics(self.since, self.until, MetricType.NUMERIC.value)
        self._fill_values(self.since, self.since + timedelta(days=2), value=value)

        datapoints = self._filter_filled_datapoints_by_user()

        self.assertEqual(4, datapoints.count())

        for datapoint in datapoints:
            self.assertEqual(self.user, datapoint.submitting_user)
            self.assertEqual(value, datapoint.numeric_value)

        self._check_empty_datapoints(
            Datapoint.objects.after(self.since + timedelta(days=2)).filter(organization=self.org_1)
        )

        copy_last_value_if_slot_was_missed()

        for datapoint in Datapoint.objects.before(self.since + timedelta(days=2)).filter(organization=self.org_1):
            self.assertEqual(value, datapoint.numeric_value)
            self.assertEqual(self.user, datapoint.submitting_user)

        autofilled_datapoints = self._filter_autofilled_datapoints()

        self.assertEqual(2, autofilled_datapoints.count())

        for datapoint in autofilled_datapoints:
            self.assertEqual(value, datapoint.numeric_value)
            self.assertEqual(self.site_config.system_user, datapoint.submitting_user)

        self._check_empty_datapoints(
            Datapoint.objects.after(self.since + timedelta(days=3)).filter(organization=self.org_1)
        )

    def test_copy_status_value_if_slot_was_missed(self):
        value = Status.RED

        self._generate_metrics(self.since, self.until, MetricType.STATUS.value)
        self._fill_values(self.since, self.since + timedelta(days=2), value=value)

        datapoints = self._filter_filled_datapoints_by_user()

        self.assertEqual(4, datapoints.count())

        for datapoint in datapoints:
            self.assertEqual(self.user, datapoint.submitting_user)
            self.assertEqual(value, datapoint.status_value)

        self._check_empty_datapoints(
            Datapoint.objects.after(self.since + timedelta(days=2)).filter(organization=self.org_1)
        )

        copy_last_value_if_slot_was_missed()

        for datapoint in Datapoint.objects.before(self.since + timedelta(days=2)).filter(organization=self.org_1):
            self.assertEqual(value, datapoint.status_value)
            self.assertEqual(self.user, datapoint.submitting_user)

        autofilled_datapoints = self._filter_autofilled_datapoints()

        self.assertEqual(2, autofilled_datapoints.count())

        for datapoint in autofilled_datapoints:
            self.assertEqual(value, datapoint.status_value)
            self.assertEqual(self.site_config.system_user, datapoint.submitting_user)

        self._check_empty_datapoints(
            Datapoint.objects.after(self.since + timedelta(days=3)).filter(organization=self.org_1)
        )

    def test_is_automatically_copy_last_value_disabled_on_organisation(self):
        self.org_1.is_automatically_copy_last_answer = False
        self.org_1.save()

        value = "Text value"

        self._generate_metrics(self.since, self.until, MetricType.TEXT.value)
        self._fill_values(self.since, self.since + timedelta(days=2), value=value)

        datapoints = self._filter_filled_datapoints_by_user()

        self.assertEqual(4, datapoints.count())

        self._check_filled_datapoints_by_user(datapoints, value)

        self._check_empty_datapoints(
            Datapoint.objects.after(self.since + timedelta(days=2)).filter(organization=self.org_1)
        )

        copy_last_value_if_slot_was_missed()

        datapoints = self._filter_filled_datapoints_by_user()

        self.assertEqual(4, datapoints.count())

        self._check_filled_datapoints_by_user(datapoints, value)

        self._check_empty_datapoints(
            Datapoint.objects.after(self.since + timedelta(days=2)).filter(organization=self.org_1)
        )

    def test_is_automatically_copy_last_value_disabled_on_question(self):
        value = "Text value"

        self._generate_metrics(self.since, self.until, MetricType.TEXT.value, is_automatically_copy_last_answer=False)
        self._fill_values(self.since, self.since + timedelta(days=2), value=value)

        datapoints = self._filter_filled_datapoints_by_user()

        self.assertEqual(4, datapoints.count())

        self._check_filled_datapoints_by_user(datapoints, value)

        self._check_empty_datapoints(
            Datapoint.objects.after(self.since + timedelta(days=2)).filter(organization=self.org_1)
        )

        copy_last_value_if_slot_was_missed()

        datapoints = self._filter_filled_datapoints_by_user()

        self.assertEqual(4, datapoints.count())

        self._check_filled_datapoints_by_user(datapoints, value)

        self._check_empty_datapoints(
            Datapoint.objects.after(self.since + timedelta(days=2)).filter(organization=self.org_1)
        )

    def test_copy_last_text_value_from_appropriate_organisation_if_slot_was_missed(self):
        value = "Text value"

        self._generate_metrics(self.since, self.until, MetricType.TEXT.value)
        self._fill_values(self.since, self.since + timedelta(days=2), value=value + '_organisation3', organization=self.org_3)
        self._fill_values(self.since, self.since + timedelta(days=2), value=value)
        datapoints = self._filter_filled_datapoints_by_user()

        self.assertEqual(4, datapoints.count())

        self._check_filled_datapoints_by_user(datapoints, value)

        self._check_empty_datapoints(
            Datapoint.objects.after(self.since + timedelta(days=2)).filter(organization=self.org_1)
        )

        copy_last_value_if_slot_was_missed()

        self.assertEqual(4, datapoints.count())

        self._check_filled_datapoints_by_user(datapoints, value)

        autofilled_datapoints = self._filter_autofilled_datapoints()
        self.assertEqual(2, autofilled_datapoints.count())

        for datapoint in autofilled_datapoints:
            self.assertEqual(self.site_config.system_user, datapoint.submitting_user)
            self.assertEqual(value, datapoint.text_value)

        self._check_empty_datapoints(
            Datapoint.objects.after(self.since + timedelta(days=3)).filter(organization=self.org_1)
        )

    @skip
    def test_copy_last_text_value_if_slot_was_missed_with_correct_deadline(self):
        self.since = timezone.now() - timedelta(days=6)
        self.until = self.since + timedelta(days=6)

        value = "Text value"
        self._generate_metrics(self.since, self.until, MetricType.TEXT.value)
        self._fill_values(self.since, self.since + timedelta(days=2), value=value)
        datapoints = self._filter_filled_datapoints_by_user()

        manually_added_value = 'manually_added_value'
        self._fill_values(
            self.since + timedelta(days=4),
            self.since + timedelta(days=4, hours=20),
            value=manually_added_value,
            count=2
        )

        manually_updated_datapoints = Datapoint.objects.filter(text_value=manually_added_value)

        self.assertEqual(1, manually_updated_datapoints.count())

        manually_updated_datapoints = manually_updated_datapoints.first()

        self.assertEqual(4, datapoints.count())

        self._check_filled_datapoints_by_user(datapoints, value)

        self._check_empty_datapoints(
            (Datapoint.objects.after(self.since + timedelta(days=2)).filter(organization=self.org_1)
             .exclude(id=manually_updated_datapoints.id))
        )

        with CaptureQueriesContext(connection) as context:
            copy_last_value_if_slot_was_missed()
            self.assertLessEqual(len(context.captured_queries), 36)

        self._check_filled_datapoints_by_user(self._filter_filled_datapoints_by_user(), value)

        self._check_filled_datapoints_by_user(
            Datapoint.objects
                .after(self.since + timedelta(days=2))
                .before(manually_updated_datapoints.timeslot.deadline)
                .filter(organization=self.org_1),
            value,
            user=self.site_config.system_user
        )
        self._check_filled_datapoints_by_user(
            Datapoint.objects.after(manually_updated_datapoints.timeslot.deadline).before(self.until)
                .filter(organization=self.org_1)[1:],
            manually_added_value,
            user=self.site_config.system_user
        )

        self._check_empty_datapoints(
            Datapoint.objects.after(self.until).filter(organization=self.org_1)
        )

    def _generate_metrics(self, since: datetime, until: datetime, metric_type: str,
                          is_automatically_copy_last_answer: bool = True) -> None:
        frequency = PollingFrequency.objects.get(identifier='twice-a-day')
        self.metric_str = Metric.objects.create(
            question='Test automatically copy last value if slot was missed',
            metric_type=metric_type,
            polling_frequency=frequency,
            is_automatically_copy_last_answer=is_automatically_copy_last_answer,
        )
        self.metric_str.organization_types.add(self.org_type)
        self.metric_str.generate_timeslots_and_datapoints(since=since, until=until)

    def _fill_values(self, since: datetime, until: datetime, value: Union[int, str],
                     organization: Organization = None, count=None) -> None:
        if not organization:
            organization = self.org_1

        minutes = 0
        for datapoint in Datapoint.objects.filter(organization=organization).after(since).before(until)[:count]:
            datapoint.submission_datetime = since + timedelta(minutes=minutes)
            minutes += 1
            datapoint.submitting_user = self.user

            if datapoint.is_numeric():
                datapoint.numeric_value = value
            elif datapoint.is_status():
                datapoint.status_value = value
            else:
                datapoint.text_value = value

            datapoint.save()

    def _check_empty_datapoints(self, datapoints: QuerySet) -> None:
        for datapoint in datapoints:
            self.assertIsNone(datapoint.submitting_user)
            self.assertIsNone(datapoint.submission_datetime)
            self.assertIsNone(datapoint.value)

    def _check_filled_datapoints_by_user(self, datapoints, expected_value, user=None):
        if not user:
            user = self.user
        for datapoint in datapoints:
            self.assertEqual(user, datapoint.submitting_user)
            self.assertEqual(expected_value, datapoint.text_value)

    def _filter_autofilled_datapoints(self):
         return (
            Datapoint.objects
                .after(self.since + timedelta(days=2))
                .before(self.since + timedelta(days=3))
                .filter(organization=self.org_1)
        )

    def _filter_filled_datapoints_by_user(self):
        return Datapoint.objects.before(self.since + timedelta(days=2)).filter(organization=self.org_1)
