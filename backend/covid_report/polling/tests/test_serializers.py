from typing import Any

from django.urls import reverse
from rest_framework.test import APITestCase

from backend.covid_report.polling.models import Datapoint
from backend.covid_report.polling.models import Metric
from backend.covid_report.polling.models import Status
from backend.covid_report.polling.models import Timeslot
from backend.covid_report.polling.tests.cases import CovidMultipleMetricsTestCase


class DatapointSerializerTest(APITestCase, CovidMultipleMetricsTestCase):

    def test_int_answer_update_1(self):
        self._test_int_update(value=5)

    def test_int_answer_update_2(self):
        self._test_int_update(value=5, comment=20)

    def test_int_answer_update_3(self):
        self._test_int_update(value=0, comment='test')

    def test_int_answer_update_4(self):
        self._test_int_update(value='24', comment='test')

    def test_int_answer_update_invalid_1(self):
        self._test_int_update(value=-5, is_invalid=True)

    def test_int_answer_update_invalid_2(self):
        self._test_int_update(value=None, is_invalid=True)

    def test_int_answer_update_invalid_3(self):
        self._test_int_update(value='', is_invalid=True)

    def test_int_answer_update_invalid_4(self):
        self._test_int_update(value='5test', is_invalid=True)

    def test_status_answer_update_green(self):
        self._test_status_update(value=Status.GREEN.value)

    def test_status_answer_update_red(self):
        self._test_status_update(value=Status.RED.value)

    def test_status_answer_update_string_1(self):
        self._test_status_update(value='0')

    def test_status_answer_update_string_2(self):
        self._test_status_update(value='2')

    def test_status_answer_update_string_3_invalid(self):
        self._test_status_update(value='50', is_invalid=True)

    def test_status_answer_update_with_comment(self):
        self._test_status_update(value=Status.RED.value, comment='Test')

    def test_status_answer_update_invalid(self):
        self._test_status_update(value=100, is_invalid=True)

    def test_status_answer_update_invalid_2(self):
        self._test_status_update(value=None, is_invalid=True)

    def test_text_answer(self):
        self._test_text_update(value="all's ok")

    def test_text_answer_invalid(self):
        self._test_text_update(value='', is_invalid=True)

    def _test_int_update(
        self, value: Any, comment: Any = '', is_invalid: bool = False,
    ):
        self._test_update(
            metric=self.metric_int, value=value, comment=comment, is_invalid=is_invalid,
        )

    def _test_text_update(
        self, value: Any, comment: Any = '', is_invalid: bool = False,
    ):
        self._test_update(
            metric=self.metric_str, value=value, comment=comment, is_invalid=is_invalid,
        )

    def _test_status_update(
        self, value: Any, comment: Any = '', is_invalid: bool = False,
    ):
        self._test_update(
            metric=self.metric_status, value=value, comment=comment, is_invalid=is_invalid,
        )

    def _test_update(
        self,
        metric: Metric,
        value: Any,
        comment: str = '',
        is_invalid: bool = False,
    ):
        self.client.force_login(self.user)
        timeslot: Timeslot = metric.timeslots.first()
        datapoint: Datapoint = timeslot.datapoints.first()
        response = self.client.patch(
            reverse('polling:json-submit', kwargs={'pk': datapoint.pk}),
            data={
                'value': value,
                'comment': comment,
            },
            format='json',
        )
        if is_invalid:
            self.assertEquals(response.status_code, 400)
        else:
            self.assertEquals(response.status_code, 200)
            datapoint_updated = Datapoint.objects.get(pk=datapoint.pk)
            if datapoint_updated.is_numeric():
                self.assertEquals(int(value), datapoint_updated.value)
            elif datapoint_updated.is_status():
                try:
                    status = Status(int(value))
                except ValueError:
                    self.fail("Invalid status")
                self.assertEquals(status, datapoint_updated.value)
            else:
                self.assertEquals(value, datapoint_updated.value)
            self.assertEquals(str(comment), datapoint_updated.comment)
