from datetime import timedelta

from django.urls import reverse
from django.utils import timezone
from freezegun import freeze_time

from backend.auth.models import User
from backend.covid_report.polling.models import Datapoint
from backend.covid_report.polling.models import Timeslot
from backend.covid_report.polling.tests.cases import CovidMultipleMetricsTestCase


class ReportingViewTest(CovidMultipleMetricsTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls._create_optional_metrics()

    def test_edit_posted_report_until_deadline(self):
        closest_timeslot: Timeslot = self.metric_int.timeslots.order_by('deadline').first()
        closest_deadline = closest_timeslot.deadline
        fifteen_min_before_deadline = closest_deadline - timedelta(minutes=15)
        
        with freeze_time(fifteen_min_before_deadline):
            response = self.client.get(
                reverse('polling:pending_for_org', kwargs={'org_slug': self.org_1.slug}),
                follow=True,
            )
            datapoint = Datapoint.objects.filter(timeslot=closest_timeslot, timeslot__metric=self.metric_int).last()
            datapoint.submitting_user = self.user
            datapoint.submission_datetime = timezone.now()
            datapoint.save()
            self.assertContains(response, self.metric_int.question)

    def test_reviewers_cant_access_frontend(self):
        reviewer = User.objects.create(
            email='test@what.digital',
            is_reviewer=True,
            is_staff=True,
        )
        reviewer.organizations.add(self.org_1)
        self.client.force_login(reviewer)
        response = self.client.get(
            reverse('polling:pending'),
            follow=True,
        )
        self.assertTemplateNotUsed(response, 'polling/pending_datapoints.html')
