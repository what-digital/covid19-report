from datetime import timedelta

from django.core import mail
from django.core.management import call_command
from django.utils import timezone
from freezegun import freeze_time

from backend.covid_report.polling.models import Datapoint
from backend.covid_report.polling.models import Timeslot
from backend.covid_report.polling.tests.cases import CovidTestCase


class ReminderEmailTest(CovidTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls._create_optional_metrics()

    def test_reminder_email(self):
        closest_timeslot: Timeslot = self.metric_int.timeslots.order_by('deadline').first()
        closest_deadline = closest_timeslot.deadline
        fifteen_min_past_deadline = closest_deadline + timedelta(minutes=15)
        with freeze_time(fifteen_min_past_deadline):
            call_command('send_email_reminders')
        self.assertEquals(len(mail.outbox), 1)

        self._submit_all_pending_datapoints(closest_timeslot)

        datapoint_submitted = Datapoint.objects.filter(timeslot=closest_timeslot).first()
        self.assertEquals(True, datapoint_submitted.is_submitted())
        
        with freeze_time(fifteen_min_past_deadline):
            call_command('send_email_reminders')
        self.assertEquals(len(mail.outbox), 1, msg="the second reminder must not be sent since the point is submitted now")

    def _submit_all_pending_datapoints(self, timeslot: Timeslot):
        datapoints_to_submit = Datapoint.objects.filter(timeslot=timeslot)
        for datapoint in datapoints_to_submit:
            datapoint.submission_datetime = timezone.now()
            datapoint.submitting_user = self.user
            datapoint.save()
