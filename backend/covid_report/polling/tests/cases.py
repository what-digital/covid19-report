from django.test import TestCase
from django.utils import translation

from backend.auth.models import Organization
from backend.auth.models import OrganizationType
from backend.auth.models import User
from backend.covid_report.polling.models import Metric
from backend.covid_report.polling.models import MetricType
from backend.covid_report.polling.models import PollingFrequency


class CovidTestCase(TestCase):
    org_type: OrganizationType

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.org_type = OrganizationType.objects.create(name="Hospital")

        cls.org_1 = Organization.objects.create(name="Basel Testing Center")
        cls.org_1.types.add(cls.org_type)
        cls.org_1.save()
        org_2 = Organization.objects.create(name="Zurich Hospital")
        org_2.types.add(cls.org_type)
        org_2.save()

        cls.user = User.objects.create(
            email="victor@what.digital",
            is_can_report=True,
        )
        cls.user.organizations.add(cls.org_1, org_2)

        translation.activate('en')

        cls.metric_int = Metric.objects.create(
            question="Number of people tested",
            metric_type=MetricType.NUMERIC.value,
            polling_frequency=PollingFrequency.objects.get(identifier='twice-a-day'),
        )
        cls.metric_int.organization_types.add(cls.org_type)
        cls.metric_int.generate_timeslots_and_datapoints()

    @classmethod
    def _create_optional_metrics(cls):
        cls.metric_int2 = Metric.objects.create(
            question="Number of people tested",
            metric_type=MetricType.NUMERIC.value,
            polling_frequency=PollingFrequency.objects.get(identifier='twice-a-day'),
            is_optional=True,
        )
        cls.metric_int2.organization_types.add(cls.org_type)
        cls.metric_int2.generate_timeslots_and_datapoints()

        cls.metric_int3 = Metric.objects.create(
            question="Number of people tested",
            metric_type=MetricType.NUMERIC.value,
            polling_frequency=PollingFrequency.objects.get(identifier='twice-a-day'),
            is_optional=True,
        )
        cls.metric_int3.organization_types.add(cls.org_type)
        cls.metric_int3.generate_timeslots_and_datapoints()


class CovidMultipleMetricsTestCase(CovidTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        frequency = PollingFrequency.objects.get(identifier='twice-a-day')
        cls.metric_str = Metric.objects.create(
            question="Describe personal well-being",
            metric_type=MetricType.TEXT.value,
            polling_frequency=frequency,
        )
        cls.metric_str.organization_types.add(cls.org_type)
        cls.metric_str.generate_timeslots_and_datapoints()

        cls.metric_status = Metric.objects.create(
            question="Available personal",
            metric_type=MetricType.STATUS.value,
            polling_frequency=frequency,
        )
        cls.metric_status.organization_types.add(cls.org_type)
        cls.metric_status.generate_timeslots_and_datapoints()

    def setUp(self):
        super().setUp()
        self.client.force_login(self.user)
