import logging
from typing import Optional

from django.utils import timezone

from backend.covid_report.polling.models import Datapoint
from backend.site_config.models import SiteConfig

logger = logging.getLogger(__name__)


def copy_last_value_if_slot_was_missed():
    logger.debug('Copy last value if slot was missed started')

    system_user = SiteConfig.get_solo().system_user

    for datapoint in Datapoint.objects.filter(
            organization__is_automatically_copy_last_answer=True,
            timeslot__metric__is_automatically_copy_last_answer=True,
            timeslot__deadline__lt=timezone.now(),
            submission_datetime__isnull=True,
    ).before(timezone.now()).select_related():
        last_answered_datapoint: Optional[Datapoint] = datapoint.organization.get_last_answered_datapoint(
            datapoint
        )
        if not last_answered_datapoint:
            continue

        if datapoint.is_numeric():
            datapoint.numeric_value = last_answered_datapoint.value
        elif datapoint.is_status():
            datapoint.status_value = last_answered_datapoint.value
        elif datapoint.is_text():
            datapoint.text_value = last_answered_datapoint.value

        datapoint.submitting_user = system_user
        datapoint.save(
            update_fields=['numeric_value', 'status_value', 'text_value', 'submitting_user',
                           'submission_datetime']
        )

    logger.debug('Copy last value if slot was missed finished successfully')
