from django.contrib.sites.models import Site
from django.db.models import QuerySet
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from import_export import fields
from import_export import resources
from import_export import widgets

from backend.covid_report.polling import models
from backend.covid_report.polling.models import Datapoint


class DatapointResource(resources.ModelResource):
    organization = fields.Field(
        attribute="organization__name", column_name=_("Organisation")
    )
    question = fields.Field(
        attribute="timeslot__metric__question", column_name=_("Question")
    )
    status_value = fields.Field(
        column_name=_("Status"),
        widget=widgets.IntegerWidget(),
    )
    numeric_value = fields.Field(
        attribute="numeric_value", column_name=_("Quantity")
    )
    text_value = fields.Field(attribute="text_value", column_name=_("Text"))
    polling_frequency = fields.Field(
        attribute="timeslot__metric__polling_frequency__name",
        column_name=_("Reporting frequency"),
    )
    deadline = fields.Field(
        attribute="timeslot__deadline",
        column_name=_("Deadline"),
        widget=widgets.DateTimeWidget(),
    )
    comment = fields.Field(attribute="comment", column_name=_("Comment"))
    question_help = fields.Field(
        attribute="timeslot__metric__help_text", column_name=_("Help text")
    )
    organisation_types = fields.Field(
        attribute="organization__types",
        column_name=_("Organisation types"),
        widget=widgets.ManyToManyWidget(
            models.Organization, separator=", ", field="name"
        ),
    )
    submitting_user = fields.Field(column_name=_("Reporter"))
    submission_datetime = fields.Field(
        attribute="submission_datetime",
        column_name=_("Reported at"),
        widget=widgets.DateTimeWidget(),
    )
    metric_url = fields.Field(column_name=_("Question URL"))
    datapoint_url = fields.Field(column_name=_("Answer URL"))

    class Meta:
        model = models.Datapoint
        fields = (
            "organization",
            "question",
            "status_value",
            "numeric_value",
            "text_value",
            "polling_frequency",
            "deadline",
            "comment",
            "question_help",
            "organisation_types",
            "submitting_user",
            "submission_datetime",
            "metric_url",
            "datapoint_url",
        )
        export_order = fields

    def get_queryset(self) -> QuerySet:
        return (
            Datapoint.objects
                .select_related(
                    'organization',
                    'timeslot__metric',
                    'timeslot__metric__polling_frequency',
                    'timeslot',
                    'submitting_user',
                )
                .prefetch_related(
                    'organization__types',
                )
        )

    def dehydrate_status_value(self, instance):
        return str(instance.status_value)

    def dehydrate_submitting_user(self, instance: Datapoint) -> str:
        user = instance.submitting_user
        if user is None:
            return ''
        name = user.get_full_name()
        if not name:
            name = user.email
        else:
            name = f"{name} ({user.email})"
        return name

    def _build_absolute_url(self, path):
        site = Site.objects.get_current()
        return f"https://{site.domain}{path}"

    def dehydrate_metric_url(self, instance):
        return self._build_absolute_url(
            reverse(
                "admin:polling_metric_change",
                args=(instance.timeslot.metric_id,),
            )
        )

    def dehydrate_datapoint_url(self, instance):
        return self._build_absolute_url(
            reverse("admin:polling_datapoint_change", args=(instance.pk,))
        )
