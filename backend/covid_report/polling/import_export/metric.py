import codecs
import csv
import logging

from backend.covid_report.polling import models
from backend.covid_report.polling.forms import FileImportForm
from backend.covid_report.polling.models import MetricType


logger = logging.getLogger(__name__)


def import_metrics_catalog(form: FileImportForm) -> int:
    fh = codecs.getreader("utf-8")(form.cleaned_data["file"])
    csv_reader = csv.reader(fh, delimiter=",", quotechar='"')

    next(csv_reader)  # Skip header

    created_questions_count = 0
    metric_to_types_mapping = {}

    for line in csv_reader:
        (
            organization_name_raw,
            type_name_raw,
            question_raw,
            question_help_text_raw,
            metric_type_raw,
            polling_frequency_raw,
            contact_raw,
        ) = [i.strip() for i in line]

        is_empty_row = not question_raw
        if is_empty_row:
            continue

        # TODO: Error handling

        type_mapping = {
            "Anzahl": models.MetricType.NUMERIC,
            "Anzahl und in Kommentarfeld Auflistung pro Altersheim": models.MetricType.NUMERIC,
            "Status Grün, gelb rot": models.MetricType.STATUS,
            "Freitext": models.MetricType.TEXT,
            "Freitext und Status, grün, gelb, rot": models.MetricType.STATUS,
        }
        metric_type: MetricType = type_mapping.get(metric_type_raw, MetricType.TEXT)
        if metric_type is None:
            logger.error(f"Unsupported metric type `{metric_type_raw}`")
            continue

        frequencies_mapping = {
            "1x täglich 0800": "once-a-day",
            "2x täglich 0800 / 1400": "twice-a-day",
            "3x täglich 0800 / 1400 / 1700": "thrice-a-day",
            "3x Wöchentlich (Mo, Mi, Fr 1400)": "thrice-a-week-pm",
            "3x Wöchentlich (Mo, Mi, Fr 0800)": "thrice-a-week-am",
        }

        polling_frequency = models.PollingFrequency.objects.get(
            identifier=frequencies_mapping[polling_frequency_raw]
        )

        metric, created = models.Metric.objects.get_or_create(
            question=question_raw,
            help_text=question_help_text_raw,
            metric_type=metric_type.value,
            polling_frequency=polling_frequency,
        )
        if created:
            created_questions_count += 1

        if type_name_raw:
            org_type, created = models.OrganizationType.objects.get_or_create(
                name=type_name_raw
            )
            if organization_name_raw:
                organization, created = models.Organization.objects.get_or_create(
                    name=organization_name_raw
                )
                if created:
                    organization.types.add(org_type)

        metric_to_types_mapping.setdefault(metric, []).append(org_type)

    for metric, types in metric_to_types_mapping.items():
        metric.organization_types.set(types)
        metric.generate_timeslots_and_datapoints()

    return created_questions_count
