from django.urls import path

from backend.covid_report.polling import views
from backend.covid_report.polling.api.views import DatapointUpdateApiView, DatapointsViewSet
from backend.covid_report.polling.views import ExportView
from backend.covid_report.polling.views import MetricHistoryView
from backend.covid_report.polling.views import ReporterIndexView
from backend.covid_report.polling.views import ReporterRedirectView


app_name = "polling"

urlpatterns = [
    path("", views.homepage, name="homepage"),
    path("pending/", ReporterRedirectView.as_view(), name="pending"),
    path("<slug:org_slug>/pending/", ReporterIndexView.as_view(), name="pending_for_org"),
    path("<slug:org_slug>/history/<int:pk>/", MetricHistoryView.as_view(), name="metric-history"),

    path("export/", ExportView.as_view(), name="export"),
    path(
        "api/submit/<int:pk>", DatapointUpdateApiView.as_view(), name="json-submit"
    ),
    path(
        "api/datapoints/", DatapointsViewSet.as_view({'get': 'list'}), name="datapoints"
    ),
    path(
        "history/", views.SubmittedMetricListView.as_view(), name="submitted-metric-list",
    ),
]
