from django.core.exceptions import ValidationError

from dateutil.rrule import rrulestr


def validate_rruleset(value):
    try:
        rrulestr(value)
    except Exception:
        raise ValidationError("Invalid recurrency rule")
