from django import forms
from django.conf import settings
from django.utils.translation import gettext_lazy as _

from . import models


class FileImportForm(forms.Form):
    file = forms.FileField()


class MetricDisablingForm(forms.Form):
    days_to_keep = forms.IntegerField(
        initial=3, min_value=0, max_value=settings.TIMESLOTS_GENERATION_PERIOD.days,
        help_text=_(
            "By default the disabled question will be demanding entries from the reporters"
            "for the next %(days)s days. You can reduce it here." % {'days': settings.TIMESLOTS_GENERATION_PERIOD.days}
        ),
        label=_("Keep existing empty questions for (days)"),
    )


class BaseDatapointForm(forms.ModelForm):
    def save(self, commit: bool = True) -> models.Datapoint:
        metric_type = self.instance.timeslot.metric.metric_type
        if metric_type == models.MetricType.NUMERIC.value:
            self.instance.numeric_value = self.cleaned_data["value"]
            self.instance.comment = self.cleaned_data["comment"].strip()
        elif metric_type == models.MetricType.STATUS.value:
            self.instance.status_value = self.cleaned_data["value"]
            self.instance.comment = self.cleaned_data["comment"].strip()
        else:
            self.instance.text_value = self.cleaned_data["value"]

        self.instance.save()
        return self.instance

    class Meta:
        model = models.Datapoint
        fields = ()


class ExportForm(forms.Form):
    format = forms.ChoiceField(label=_("Format"), choices=(), required=False)

    def __init__(self, formats, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["format"].choices = [
            (f().get_format().title, f().get_title()) for f in formats
        ]

    def clean_format(self):
        file_format = self.cleaned_data["format"]
        if not file_format:
            return 0
        else:
            for i, f in enumerate(self.fields["format"].choices):
                if f[0] == file_format:
                    return i
            else:
                raise forms.ValidationError(f"Invalid format: {file_format}")

    def clean(self):
        data = super().clean()
        try:
            data["file_format"] = data.pop("format")
        except KeyError:
            pass
        return data
