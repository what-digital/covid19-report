from django.core.management.base import BaseCommand

from backend.covid_report.polling import utils


class Command(BaseCommand):
    def handle(self, *args, **options):
        utils.copy_last_value_if_slot_was_missed()
