from datetime import timedelta

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import timezone
from django.utils import translation
from django.utils.translation import pgettext
from djangocms_helpers.utils.send_email import send_email

from backend.auth.models import User
from backend.covid_report.polling.models import Datapoint
from backend.covid_report.polling.models import Metric


class Command(BaseCommand):
    def handle(self, *args, **options):
        for user in User.objects.filter(is_can_report=True):
            metric_pks = (
                Datapoint.objects.unsubmitted()
                .compulsory()
                .filter(
                    organization__in=user.get_accessible_organizations(),
                    organization__is_enable_reminder_emails=True,
                    timeslot__deadline__lt=timezone.now(),
                    timeslot__deadline__gte=timezone.now() - timedelta(hours=3),
                )
                .values_list("timeslot__metric_id", flat=True)
                .distinct()
            )
            metrics = Metric.objects.filter(pk__in=metric_pks)
            is_has_missed_deadlines = bool(metrics)
            if is_has_missed_deadlines:
                translation.activate('de')
                send_email(
                    email_subject=pgettext(
                        'reminder',
                        "KKS Basel-Landschaft: Please urgently enter the current data!"
                    ),
                    email_destination=user.email,
                    email_reply_to=settings.DEFAULT_REPLY_TO_EMAIL,
                    template_path_without_extension="polling/emails/reminder_email",
                    template_context={'metrics': metrics},
                )
