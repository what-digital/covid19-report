import datetime
from typing import List
from typing import Optional

import attr
import pytz
from basicauth.decorators import basic_auth_required
from dateutil import parser
from django.conf import settings
from django.contrib import messages
from django.db.models import Q
from django.db.models import QuerySet
from django.http import HttpRequest
from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.functional import cached_property
from django.utils.translation import pgettext
from django.views import View
from django.views.generic import ListView
from django.views.generic import TemplateView
from import_export.mixins import ExportViewFormMixin

from backend.auth.mixins import OrganizationAccessMixin
from backend.auth.mixins import OrganizationRequiredMixin
from backend.auth.mixins import ReporterRequiredMixin
from backend.auth.mixins import ViewerRequiredMixin
from backend.auth.mixins import login_required_error
from backend.auth.models import Organization
from backend.covid_report.polling import forms
from backend.covid_report.polling import models
from backend.covid_report.polling import utils
from backend.covid_report.polling.import_export.datapoint import (
    DatapointResource,
)
from backend.covid_report.polling.middleware import Redirect
from backend.covid_report.polling.models import Datapoint
from backend.covid_report.polling.models import Metric


def homepage(request: HttpRequest) -> HttpResponseRedirect:
    if request.user.is_anonymous:
        raise Redirect("account_login", login_required_error)
    elif request.user.is_can_report:
        return redirect("polling:pending")
    elif request.user.is_can_view:
        return redirect("polling:submitted-metric-list")
    elif request.user.is_superuser or request.user.is_staff:
        return redirect("polling:pending")
    elif request.user.is_reviewer:
        return redirect('admin:index')
    else:
        messages.error(
            request,
            pgettext(
                "auth", "You are not allowed to access the reports pages.",
            ),
        )
        return redirect(settings.LOGIN_URL)


@attr.s
class OrganizationMetric:
    organization: models.Organization = attr.ib()
    metric: models.Metric = attr.ib()
    _timestamp: datetime.datetime = attr.ib(factory=timezone.now)

    def datapoints(self):
        return models.Datapoint.objects.filter(
            organization=self.organization, timeslot__metric=self.metric
        )

    @cached_property
    def past_datapoints(self):
        return self.datapoints().before(self._timestamp)

    @cached_property
    def future_datapoints(self) -> QuerySet:
        return self.datapoints().after(self._timestamp)

    @cached_property
    def overdue_datapoints(self):
        qs = self.past_datapoints.unsubmitted()
        return list(
            qs.optional(
                self.metric,
                self.past_datapoints.last_optional(self.metric)
            )
        ) + list(
            qs.compulsory()
        )

    @cached_property
    def last_entry(self):
        return self.past_datapoints.submitted().last()

    @cached_property
    def next_datapoint(self) -> Optional[Datapoint]:
        return self.future_datapoints.first()

    def to_fill(self) -> List[Datapoint]:
        if self.metric.is_optional:
            if self.next_datapoint:
                return [self.next_datapoint]
        else:
            if self.next_datapoint:
                return self.overdue_datapoints + [self.next_datapoint]
            else:
                return self.overdue_datapoints


class ReporterRedirectView(ReporterRequiredMixin, OrganizationRequiredMixin, View):
    def get(self, request: HttpRequest) -> HttpResponse:
        return redirect(
            'polling:pending_for_org',
            org_slug=request.user.get_accessible_organizations().first().slug,
        )


class ReporterIndexView(
    ReporterRequiredMixin, OrganizationRequiredMixin, OrganizationAccessMixin, TemplateView
):
    template_name = "polling/pending_datapoints.html"

    def get_context_data(self, **kwargs):
        org_current = Organization.objects.get(slug=kwargs.get('org_slug'))
        now = timezone.now()
        metric_ids = (
            models.Datapoint.objects
                .filter(
                Q(submission_datetime__isnull=True, timeslot__deadline__lt=now)
                | Q(timeslot__deadline__gte=now)
            )
                .filter(organization=org_current)
                .values_list("timeslot__metric_id", flat=True)
                .distinct()
        )
        metrics = models.Metric.objects.filter(pk__in=metric_ids)
        return {
            **super().get_context_data(**kwargs),
            "org_current": org_current,
            "org_list": self.request.user.get_accessible_organizations(),
            "metrics": [
                OrganizationMetric(org_current, m, now) for m in metrics
            ],
        }


class SubmittedMetricListView(
    ViewerRequiredMixin, OrganizationRequiredMixin, OrganizationAccessMixin, ListView
):
    model = models.Metric
    template_name = "polling/submitted_metric_list.html"

    def get_context_data(self, **kwargs):
        organization = self.request.user.organization
        now = timezone.now()

        metric_ids = (
            models.Datapoint.objects.submitted()
                .filter(organization=organization)
                .values_list("timeslot__metric_id", flat=True)
                .distinct()
        )
        metrics = models.Metric.objects.filter(pk__in=metric_ids)
        return {
            **super().get_context_data(**kwargs),
            "metrics": [
                OrganizationMetric(organization, m, now) for m in metrics
            ],
        }


class MetricHistoryView(
    ViewerRequiredMixin, OrganizationRequiredMixin, OrganizationAccessMixin, TemplateView,
):
    template_name = "polling/metric_history.html"

    def get_context_data(self, **kwargs):
        self.object = Metric.objects.get(pk=kwargs['pk'])
        org_current = Organization.objects.get(slug=kwargs.get('org_slug'))
        return {
            "org_current": org_current,
            "org_metric": OrganizationMetric(org_current, self.object),
            "org_list": self.request.user.get_accessible_organizations().order_by('name'),
            "object": self.object,
        }


@method_decorator(basic_auth_required, name='dispatch')
class ExportView(ExportViewFormMixin, ListView):
    model = models.Datapoint
    resource_class = DatapointResource
    form_class = forms.ExportForm

    def get_queryset(self) -> List[Datapoint]:
        queryset: QuerySet = (
            super().get_queryset()
                .submitted()
                .select_related(
                'organization',
                'timeslot__metric',
                'timeslot__metric__polling_frequency',
                'timeslot',
                'submitting_user',
            )
                .prefetch_related(
                'organization__types',
            )
        )
        date_start = self.get_date_start()
        # max_days = self.get_max_days()
        max_days = None

        if date_start:
            date_start = date_start

            if max_days:
                end_date = date_start + datetime.timedelta(days=int(max_days) - 1, hours=23, minutes=59, seconds=59)
                end_date = end_date
                queryset = queryset.filter(submission_datetime__gte=date_start, submission_datetime__lte=end_date)

            else:
                queryset = queryset.filter(submission_datetime__gte=date_start)

        # there's a bug in iterator that bypasses prefetch_related
        # that's why we iterate here instead
        # perhaps related to django-import-export#774
        return [instance for instance in queryset]

    def get_form_kwargs(self):
        return {
            'initial': self.get_initial(),
            'data': self.request.GET,
            'formats': self.get_export_formats(),
        }

    def form_invalid(self, form):
        return HttpResponseBadRequest()

    def get(self, request, *args, **kwargs):
        utils.copy_last_value_if_slot_was_missed()

        self.object_list = self.get_queryset()
        return super().post(request, *args, **kwargs)

    def get_date_start(self) -> Optional[datetime.datetime]:
        start_date_raw: str = self.request.GET.get('start_date', None)
        if start_date_raw:
            return parser.parse(start_date_raw)
        else:
            return None

    def get_max_days(self) -> Optional[int]:
        return self.request.GET.get('max_days', None)
