from datetime import datetime

from django import forms
from django.utils.translation import pgettext

from backend.auth.models import User


class DatapointAdminForm(forms.ModelForm):
    user: User

    def clean(self):
        if not self.instance.is_optional and self.is_value_field_is_empty():
            raise forms.ValidationError(pgettext(
                "admin__datapoint", "The value is required for non optional questions."
            ))
        return super().clean()

    def save(self, commit=True):
        super().save(commit)
        if self.user.is_reviewer:
            self.instance.submitting_user = self.user
            self.instance.submission_datetime = datetime.now()
            self.instance.save()
        return self.instance

    def is_value_field_is_empty(self):
        value_fields = ["status", "text", "numeric"]
        for field in value_fields:
            value = self.cleaned_data.get(f"{field}_value", None)
            if value is not None and value != "":
                return False
        return True
