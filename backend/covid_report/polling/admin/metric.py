from datetime import timedelta
from typing import List

from adminsortable2.admin import SortableAdminMixin
from adminutils import form_processing_action
from adminutils import options
from adminutils import queryset_action
from django.contrib import admin
from django.contrib import messages
from django.contrib.admin import ModelAdmin
from django.http import HttpRequest
from django.http import HttpResponse
from django.shortcuts import redirect
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django_object_actions import DjangoObjectActions

from backend.covid_report.polling import forms
from backend.covid_report.polling import models
from backend.covid_report.polling.forms import MetricDisablingForm
from backend.covid_report.polling.import_export.metric import import_metrics_catalog
from backend.covid_report.polling.models import Metric


@admin.register(models.Metric)
class MetricAdmin(SortableAdminMixin, DjangoObjectActions, ModelAdmin):
    search_fields = ("question",)
    list_display = (
        "question",
        "metric_type",
        "polling_frequency",
        "is_active",
        "is_automatically_copy_last_answer",
        "is_optional",
        "is_enable_comment",
        "organization_types_list",
    )
    list_filter = (
        "is_active",
        "is_optional",
        "is_automatically_copy_last_answer",
        "is_enable_comment",
        "metric_type",
        "polling_frequency",
        "organization_types",
    )
    filter_horizontal = ("organization_types",)
    changelist_actions = ("import_metrics_catalog",)
    actions = ("generate_timeslots_and_datapoints",)
    change_actions = [
        "metric_disable",
    ]

    def get_change_actions(self, request, object_id, form_url) -> List[str]:
        is_valid_id = type(object_id) == int
        if is_valid_id:
            metric: Metric = Metric.objects.get(pk=object_id)
            if metric.is_active:
                return self.change_actions
        return []

    def organization_types_list(self, obj: models.Metric) -> str:
        if obj.organization_types.exists():
            types_list = [type.name for type in obj.organization_types.all()]
            types_list = ", ".join(types_list)
            return types_list
        else:
            return ""

    @options(label=_("Deactivate"))
    @form_processing_action(form_class=MetricDisablingForm, action_label=_("Deactivate"))
    def metric_disable(self, request: HttpRequest, form: MetricDisablingForm) -> HttpResponse:
        metric_pk = request.resolver_match.kwargs.get('pk')
        metric: Metric = Metric.objects.get(pk=metric_pk)
        result: dict = metric.generate_timeslots_and_datapoints(
            since=timezone.now(),
            until=timezone.now() + timedelta(days=form.cleaned_data['days_to_keep']),
        )
        metric.is_active = False
        metric.save()
        messages.success(
            request,
            _(
                "%(timeslots)s future timeslots deleted, %(datapoints)s empty answers deleted." %
                {'timeslots': result.get('deleted_timeslots'), 'datapoints': result.get('deleted_datapoints')},
            ),
        )
        return redirect('admin:polling_metric_changelist')

    @options(label=_("Import questions catalog"), attrs={"use_form": False})
    @form_processing_action(
        form_class=forms.FileImportForm, action_label=_("Import"),
    )
    def import_metrics_catalog(self, request, form: forms.FileImportForm):
        questions_count = import_metrics_catalog(form)
        messages.success(
            request, _("Created {} new questions.").format(questions_count),
        )

    @options(
        label=_("Generate timeslots"),
        desc=_("Generate timeslots for the selected questions"),
    )
    @queryset_action
    def generate_timeslots_and_datapoints(self, request, queryset):
        for metric in queryset.iterator():
            metric.generate_timeslots_and_datapoints()
