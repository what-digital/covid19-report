import logging
import operator
import urllib
from datetime import datetime
from datetime import timedelta
from typing import Iterable
from typing import List

from adminutils import ModelAdmin
from adminutils import options
from dataclasses import dataclass
from django.contrib import admin
from django.contrib import messages
from django.contrib.admin import SimpleListFilter
from django.contrib.admin.views.main import ChangeList
from django.db.models import Q
from django.db.models import QuerySet
from django.http import HttpRequest
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import path
from django.urls import reverse
from django.urls.resolvers import RoutePattern
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext
from enumfields import Enum
from import_export.admin import ExportActionMixin

from backend.auth.models import Organization
from backend.auth.models import User
from backend.covid_report.polling import models
from backend.covid_report.polling.admin.forms import DatapointAdminForm
from backend.covid_report.polling.import_export.datapoint import DatapointResource
from backend.covid_report.polling.managers import DatapointQuerySet
from backend.covid_report.polling.middleware import Redirect
from backend.covid_report.polling.models import Datapoint
from backend.covid_report.polling.models import Metric
from backend.covid_report.polling.models import MetricType
from backend.covid_report.polling.models import Timeslot


logger = logging.getLogger(__name__)


class SubmittedFilterEnum(Enum):
    SUBMITTED = None
    UNSUBMITTED = 'unsubmitted'
    ALL = 'all'
    
    class Labels:
        SUBMITTED = pgettext('admin', "Yes")
        UNSUBMITTED = pgettext('admin', "No")
        ALL = pgettext('admin', "All")
        

class SubmissionFilter(SimpleListFilter):
    title = _('Submitted')
    parameter_name = 'is_submitted'

    def choices(self, change_list: ChangeList) -> Iterable:
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == lookup,
                'query_string': change_list.get_query_string(
                    {self.parameter_name: lookup}, [],
                ),
                'display': title,
            }

    def lookups(self, request: HttpRequest, model_admin: 'DatapointAdmin') -> list:
        return SubmittedFilterEnum.choices()

    def queryset(self, request: HttpRequest, queryset: DatapointQuerySet) -> DatapointQuerySet:
        if self.value() == SubmittedFilterEnum.SUBMITTED.value:
            return queryset.submitted()
        elif self.value() == SubmittedFilterEnum.UNSUBMITTED.value:
            return queryset.unsubmitted()
        elif self.value() is SubmittedFilterEnum.ALL.value:
            return queryset


class OrganizationFilter(admin.SimpleListFilter):
    title = _("Organization")
    parameter_name = 'organization'

    def lookups(self, request, model_admin):
        if request.user.is_reviewer:
            orgs = Organization.objects.filter(
                Q(types__pk__in=request.user.organization_types.all()) |
                Q(pk__in=request.user.organizations.all())
            ).distinct()
        else:
            orgs = Organization.objects.all()
        return ((org.id, org.name) for org in orgs)

    def queryset(self, request, queryset):
        org_pk = self.value()
        return queryset.filter(organization__pk=org_pk) if org_pk else queryset


class ReporterFilter(admin.SimpleListFilter):
    title = _("Reporter")
    parameter_name = 'submitting_user'

    def lookups(self, request, model_admin):
        if request.user.is_reviewer:
            users = (
                User.objects
                    .filter(
                        Q(organization_types__pk__in=request.user.organization_types.all()) |
                        Q(organizations__types__pk__in=request.user.organization_types.all()) |
                        Q(organizations__pk__in=request.user.organizations.all()),
                    )
                    .distinct()
            )
        else:
            users = User.objects.all()
        return ((user.id, user.email) for user in users)

    def queryset(self, request, queryset):
        user_pk = self.value()
        return queryset.filter(submitting_user__pk=user_pk) if user_pk else queryset


class MetricFilter(admin.SimpleListFilter):
    title = _("Question")
    parameter_name = 'timeslot__metric'

    def lookups(self, request, model_admin):
        if request.user.is_reviewer:
            datapoints = (
                Datapoint.objects
                    .filter(
                        Q(organization__types__pk__in=request.user.organization_types.all()) |
                        Q(organization__pk__in=request.user.organizations.all()),
                        submission_datetime__isnull=False,
                    )
                    .distinct()
            )
            metrics = Metric.objects.filter(timeslots__datapoints__in=datapoints).distinct()
        else:
            metrics = Metric.objects.all()
        return ((metric.id, metric.question) for metric in metrics)

    def queryset(self, request, queryset):
        metric_pk = self.value()
        return queryset.filter(timeslot__metric__pk=metric_pk) if metric_pk else queryset


@admin.register(models.Datapoint)
class DatapointAdmin(ExportActionMixin, ModelAdmin):
    list_display = [
        "metric",
        "deadline",
        "organization",
        "status_value",
        "numeric_value",
        "text_value",
        "submitting_user",
        "submission_datetime",
    ]
    date_hierarchy = "timeslot__deadline"
    search_fields = [
        "timeslot__metric__question",
        "organization__name"
    ]
    list_filter = [
        SubmissionFilter,
        OrganizationFilter,
        "timeslot__metric__metric_type",
        "submission_datetime",
        "status_value",
        ReporterFilter,
        MetricFilter,
    ]
    form = DatapointAdminForm
    resource_class = DatapointResource
    actions = [
        'export_admin_action',
    ]
    
    ordering = [
        '-timeslot__deadline'
    ]

    _export_selected_pks_name = 'export_selected_pks'

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        form.user = request.user
        return form

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        """
        By default django tries to fetch all time slots, but their amount keeps growing,
        soon enough it won't be possible to load this page because of the http timeout.
        """
        if db_field.name == "timeslot":
            datapoint_id = request.resolver_match.kwargs.get('object_id')
            is_edit_view = bool(datapoint_id)
            if is_edit_view:
                datapoint = Datapoint.objects.get(pk=datapoint_id)
                timeslots_closest = Timeslot.objects.filter(metric=datapoint.timeslot.metric)
            else:
                timeslots_closest = Timeslot.objects.filter(
                    deadline__gte=datetime.now() - timedelta(days=90)
                )
            kwargs["queryset"] = timeslots_closest
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    @options(desc=_("question"), order="timeslot__metric__question")
    def metric(self, obj):
        return obj.timeslot.metric.question

    @options(desc=_("deadline"), order="timeslot__deadline")
    def deadline(self, obj: Datapoint) -> datetime:
        return obj.timeslot.deadline

    def get_queryset(self, request: HttpRequest) -> QuerySet:
        if self._is_has_reviewer_perms(request):
            return (
                Datapoint.objects
                    .filter(
                        Q(organization__types__pk__in=request.user.organization_types.all()) |
                        Q(organization__pk__in=request.user.organizations.all()),
                    )
                    .distinct()
                    .order_by(*self.ordering)
            )
        else:
            return super().get_queryset(request)

    def get_readonly_fields(
        self,
        request: HttpRequest,
        obj: Datapoint = None,
    ) -> List[str]:
        if not obj:
            return []
        if obj.timeslot.metric.metric_type == MetricType.STATUS.value:
            readonly_fields = [
                "numeric_value",
                "text_value",
            ]
        elif obj.timeslot.metric.metric_type == MetricType.TEXT.value:
            readonly_fields = [
                "numeric_value",
                "status_value",
            ]
        elif obj.timeslot.metric.metric_type == MetricType.NUMERIC.value:
            readonly_fields = [
                "status_value",
                "text_value",
            ]
        else:
            logger.error(f"Type {obj.timeslot.metric.metric_type} isn't supported")
            readonly_fields = []

        if request.user.is_reviewer:
            readonly_fields.extend([
                "timeslot",
                "organization",
                "submitting_user",
                "submission_datetime",
            ])

        return readonly_fields

    def export_admin_action(
        self, request: HttpRequest, queryset: DatapointQuerySet = None,
    ) -> HttpResponse:
        if self._is_has_missing_data(queryset):
            request.session[self._export_selected_pks_name] = [
                datapoint.pk for datapoint in queryset
            ]
            return self._admin_datapoint_export_confirmation(request, queryset)
        else:
            return super().export_admin_action(request, queryset)

    def export_confirmed_admin_action(self, request: HttpRequest) -> HttpResponse:
        if self._export_selected_pks_name not in request.session:
            messages.error(request, "An error occurred. Please try to run the export again or contact the support.")
            raise Redirect('admin:index')
        queryset = Datapoint.objects.filter(
            pk__in=request.session[self._export_selected_pks_name]
        )
        return super().export_admin_action(request, queryset=queryset)

    def get_urls(self) -> List[RoutePattern]:
        urls = super().get_urls()
        additional_urls = [
            path(
                "export/",
                self.admin_site.admin_view(self.export_confirmed_admin_action),
                name='polling_datapoint_export_confirmed',
            ),
            path(
                "export-confirmation/",
                self.admin_site.admin_view(self._admin_datapoint_export_confirmation),
                name='polling_datapoint_export_confirmation',
            ),
        ]
        return additional_urls + urls

    def has_change_permission(self, request, obj=None) -> bool:
        if self._is_has_reviewer_perms(request):
            return self._is_can_manage(reviewer=request.user, datapoint=obj)
        else:
            return super().has_change_permission(request, obj)

    def has_view_permission(self, request, obj=None) -> bool:
        if request.user.is_reviewer:
            return True
        else:
            return super().has_view_permission(request, obj)

    def has_module_permission(self, request) -> bool:
        if request.user.is_reviewer:
            return True
        else:
            return super().has_module_permission(request)

    def _admin_datapoint_export_confirmation(
        self, request: HttpRequest, queryset: DatapointQuerySet = None,
    ) -> HttpResponse:
        if not queryset:
            queryset = Datapoint.objects.all()
        org_with_missing_data_list: List[OrganizationWithMissingData] = []
        org_with_missing_data_pk_list: List[int] = (
            queryset
                .unsubmitted()
                .filter(timeslot__deadline__lte=timezone.now())
                .order_by("organization")
                .distinct("organization__name")
                .values_list("organization", flat=True)
        )
        for org_pk in org_with_missing_data_pk_list:
            missing_datapoints_count = (
                queryset
                    .filter(organization_id=org_pk)
                    .unsubmitted()
                    .filter(timeslot__deadline__lte=timezone.now())
                    .distinct()
                    .count()
            )
            filter_by_question_and_deadline = dict(o="1.2")
            query_filter = dict(
                submission_datetime__isnull=True,
                timeslot__deadline__lte=timezone.now(),
                organization_id=org_pk,
                **filter_by_question_and_deadline,
            )
            query_encoded = urllib.parse.urlencode(query_filter)
            org_with_missing_data = OrganizationWithMissingData(
                organization=Organization.objects.get(pk=org_pk),
                admin_url=f"{reverse('admin:polling_datapoint_changelist')}?{query_encoded}",
                missing_datapoints_count=missing_datapoints_count,
            )
            org_with_missing_data_list.append(org_with_missing_data)
        org_with_missing_data_list.sort(
            key=operator.attrgetter('missing_datapoints_count'),
            reverse=True,
        )

        # noinspection PyCallByClass,PyTypeChecker
        return render(
            request,
            context={
                'org_with_missing_data_list': org_with_missing_data_list,
                'file_format': request.POST.get('file_format', ''),
            },
            template_name="polling/admin/export_confirmation.html",
        )

    def _is_has_reviewer_perms(self, request: HttpRequest) -> bool:
        return request.user.is_reviewer and not request.user.is_superuser

    def _is_can_manage(self, reviewer: User, datapoint: Datapoint = None) -> bool:
        if not datapoint:
            return False
        return (
            Datapoint.objects
                .filter(
                    Q(organization__types__pk__in=reviewer.organization_types.all()) |
                    Q(organization__pk__in=reviewer.organizations.all()),
                    pk=datapoint.pk,
                )
                .exists()
        )

    def _is_has_missing_data(self, queryset: DatapointQuerySet = None) -> bool:
        return queryset.unsubmitted().exists()


@dataclass
class OrganizationWithMissingData:
    organization: Organization
    missing_datapoints_count: int
    admin_url: str
