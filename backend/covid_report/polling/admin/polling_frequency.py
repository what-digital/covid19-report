from django.contrib import admin

from backend.covid_report.polling.models import PollingFrequency


@admin.register(PollingFrequency)
class PollingFrequencyAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "identifier",
    )
