from adminutils import options
from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from backend.covid_report.polling import models


@admin.register(models.Timeslot)
class TimeslotAdmin(admin.ModelAdmin):
    date_hierarchy = "deadline"
    search_fields = ("metric__question",)
    list_display = ("metric", "deadline", "organization_types_list")
    list_filter = (
        "metric",
        "deadline",
    )

    @options(desc=_("organisation types"))
    def organization_types_list(self, obj: models.Timeslot) -> str:
        return ", ".join(
            obj.metric.organization_types.values_list("name", flat=True)
        )
