import enum
from typing import Type

from dateutil.rrule import rrulestr
from django import forms
from django.conf import settings
from django.db import models
from django.db import transaction
from django.forms import Form
from django.utils import timezone
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext
from enumfields import Enum
from enumfields import EnumField

from backend.auth.models import Organization
from backend.auth.models import OrganizationType
from backend.auth.models import User
from .managers import DatapointQuerySet
from .validators import validate_rruleset


class MetricType(enum.Enum):
    STATUS = 0
    TEXT = 1
    NUMERIC = 2


METRIC_TYPES = [
    (MetricType.STATUS.value, _("Status")),
    (MetricType.TEXT.value, _("Text")),
    (MetricType.NUMERIC.value, _("Numeric")),
]


class Status(Enum):
    RED = 0
    YELLOW = 1
    GREEN = 2

    class Labels:
        RED = _("Red")
        YELLOW = _("Yellow")
        GREEN = _("Green")


class FieldVisibility(enum.Enum):
    HIDDEN = 0
    OPTIONAL = 1
    REQUIRED = 2


COMPILATION_CHOICES = [
    (FieldVisibility.HIDDEN.value, _("Hidden")),
    (FieldVisibility.OPTIONAL.value, _("Optional")),
    (FieldVisibility.REQUIRED.value, _("Required")),
]


class PollingFrequency(models.Model):
    name = models.CharField(_("name"), max_length=512)
    identifier = models.SlugField(
        _("identifier"),
        null=True,
        blank=True,
        unique=True,
        help_text=_(
            "An optional identifier to reference this frequency in code."
        ),
    )
    schedule_str = models.TextField(
        _("schedule"),
        validators=[validate_rruleset],
        help_text=_("RFC2445-compliant recurrency rule specification."),
    )

    class Meta:
        verbose_name = _("polling frequency")
        verbose_name_plural = _("polling frequencies")

    @cached_property
    def schedule(self):
        return self.get_schedule()

    def get_schedule(self):
        return rrulestr(self.schedule_str, forceset=True, compatible=True)

    def __str__(self):
        return self.name


class Metric(models.Model):
    question = models.CharField(_("question"), max_length=1024)
    help_text = models.TextField(_("help text"), blank=True)
    organization_types = models.ManyToManyField(OrganizationType)
    polling_frequency = models.ForeignKey(
        PollingFrequency,
        on_delete=models.PROTECT,
        verbose_name=_("polling frequency"),
    )
    order = models.PositiveIntegerField(default=0)
    is_active = models.BooleanField(
        default=True,
        verbose_name=_("active"),
        help_text=_("Deactivation prevents new time slots generation, the existing unfilled slots remain in place."),
    )
    is_enable_comment = models.BooleanField(
        default=True,
        verbose_name=_("enable comment field"),
    )

    metric_type = models.PositiveSmallIntegerField(choices=METRIC_TYPES)
    is_optional = models.BooleanField(default=False)
    is_automatically_copy_last_answer = models.BooleanField(
        default=False,
        verbose_name=_("Autofill"),
        help_text=_("Automatically copy the last value if the slot was missed"),
    )

    class Meta:
        verbose_name = pgettext("admin__metric", "question")
        verbose_name_plural = pgettext("admin__metric", "questions")
        ordering = ["order"]

    def __str__(self):
        return self.question

    def has_status(self) -> bool:
        return MetricType(self.metric_type) is MetricType.STATUS

    def has_number(self) -> bool:
        return MetricType(self.metric_type) is MetricType.NUMERIC

    def has_text(self) -> bool:
        return True

    @property
    def organizations(self):
        return Organization.objects.filter(
            types__in=self.organization_types.all()
        ).distinct()

    def generate_timeslots_and_datapoints(self, *, since=None, until=None) -> dict:
        if not self.is_active:
            return {}

        if since is None:
            since = timezone.now()
        if until is None:
            until = since + settings.TIMESLOTS_GENERATION_PERIOD

        with transaction.atomic():
            organization_ids = self.organizations.values_list("pk", flat=True)

            timeslots_to_keep = []
            datapoints_to_keep = []
            created_timeslots = 0
            created_datapoints = 0

            for deadline in self.polling_frequency.schedule.between(
                since, until
            ):
                timeslot, created = self.timeslots.get_or_create(
                    deadline=deadline
                )
                timeslots_to_keep.append(timeslot.pk)
                if created:
                    created_datapoints += 1

                for org_id in organization_ids:
                    datapoint, created = timeslot.datapoints.get_or_create(
                        organization_id=org_id
                    )
                    datapoints_to_keep.append(datapoint.pk)
                    if created:
                        created_datapoints += 1

            # Delete all future unsubmitted datapoints
            deleted_datapoints, _ = Datapoint.objects.filter(
                timeslot__metric=self, timeslot__deadline__gt=since
            ).exclude(pk__in=datapoints_to_keep).unsubmitted().delete()

            # Delete timeslots without datapoints
            deleted_timeslots, _ = self.timeslots.filter(
                deadline__gt=since, datapoints__isnull=True
            ).exclude(pk__in=timeslots_to_keep).delete()

            return {
                "created_timeslots": created_timeslots,
                "created_datapoints": created_datapoints,
                "deleted_timeslots": deleted_timeslots,
                "deleted_datapoints": deleted_datapoints,
            }

    def get_form_class(self) -> Type:
        from .forms import BaseDatapointForm

        mt = MetricType(self.metric_type)
        comment_field = forms.CharField(
            required=False,
            widget=forms.Textarea(
                attrs={
                    "placeholder": _("Add your comment here..."),
                    "rows": 3,
                },
            ),
        )

        if mt is MetricType.NUMERIC:
            value_field = forms.IntegerField(min_value=0, required=True)
        elif mt is MetricType.STATUS:
            value_field = EnumField(Status, max_length=64).formfield()
        else:
            value_field = forms.CharField(
                required=True,
                widget=forms.Textarea(
                    attrs={
                        "placeholder": _("Enter your answer"),
                        "rows": 1,
                    }
                ),
            )
            comment_field = None

        fields = {
            "value": value_field,
        }
        if comment_field:
            fields["comment"] = comment_field

        form_class = type("DatapointForm", (BaseDatapointForm,), fields)
        return form_class


class Timeslot(models.Model):
    metric = models.ForeignKey(
        Metric,
        on_delete=models.CASCADE,
        verbose_name=_("question"),
        related_name="timeslots",
    )
    deadline = models.DateTimeField()

    def __str__(self):
        return f"{self.metric} / {self.deadline.date().isoformat()}"

    class Meta:
        unique_together = ("metric", "deadline")
        ordering = ["metric", "deadline"]


class Datapoint(models.Model):
    timeslot = models.ForeignKey(
        Timeslot, on_delete=models.PROTECT, related_name="datapoints"
    )
    organization = models.ForeignKey(Organization, on_delete=models.PROTECT)

    # Answer
    numeric_value = models.PositiveIntegerField(null=True, blank=True)
    status_value = EnumField(Status, null=True, blank=True, max_length=64)
    text_value = models.CharField(max_length=16384, blank=True)

    # Metadata
    comment = models.TextField(blank=True)
    submitting_user = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        null=True,
        blank=True,
        verbose_name=_("reporter"),
    )
    submission_datetime = models.DateTimeField(
        null=True, blank=True, verbose_name=_("reported at"),
    )

    objects = DatapointQuerySet.as_manager()

    class Meta:
        verbose_name = pgettext("admin__datapoint", "answer")
        verbose_name_plural = pgettext("admin__datapoint", "Answers")
        unique_together = ("timeslot", "organization")
        ordering = ("timeslot__deadline",)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.submission_datetime and self.submitting_user:
            self.submission_datetime = timezone.now()
        super().save(force_insert, force_update, using, update_fields)

    def is_type_numeric(self) -> bool:
        return self.numeric_value is not None

    def is_type_status(self) -> bool:
        return self.status_value is not None

    def is_submitted(self):
        return self.submission_datetime is not None

    def is_overdue(self, now=None):
        if self.is_submitted():
            return False
        if now is None:
            now = timezone.now()
        return self.timeslot.deadline < now

    def is_optional(self):
        return self.timeslot.metric.is_optional

    def is_upcoming(self, now=None):
        """
        Returns true if this datapoint is the next expiring one.
        """
        if now is None:
            now = timezone.now()
        return self == self.__class__.objects.filter(
            organization_id=self.organization_id,
            timeslot__metric_id=self.timeslot.metric_id,
        ).get_upcoming(now)

    def is_editable(self, now=None):
        return self.is_overdue(now) or self.is_upcoming(now)

    def get_status_value_class(self):
        if self.status_value is None:
            return "secondary"
        return {
            Status.GREEN: "success",
            Status.YELLOW: "warning",
            Status.RED: "danger",
        }[Status(self.status_value)]

    @property
    def value(self):
        if self.numeric_value is not None:
            return self.numeric_value
        if self.status_value is not None:
            return self.status_value
        if self.text_value:
            return self.text_value
        return None

    def is_numeric(self) -> bool:
        if MetricType(self.timeslot.metric.metric_type) is MetricType.NUMERIC:
            return True
        return False

    def is_status(self) -> bool:
        if MetricType(self.timeslot.metric.metric_type) is MetricType.STATUS:
            return True
        return False

    def is_text(self) -> bool:
        if MetricType(self.timeslot.metric.metric_type) is MetricType.TEXT:
            return True
        return False

    def get_form(self, **kwargs) -> Form:
        kwargs["instance"] = self
        kwargs["initial"] = {
            "value": self.value,
            "comment": self.comment,
        }
        form: Form = self.timeslot.metric.get_form_class()(**kwargs)
        if self.value is None:
            form.is_filled = False
        else:
            form.is_filled = True
        return form

    def __str__(self):
        return f"{self.timeslot} / value={self.value}"
