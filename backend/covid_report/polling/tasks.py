from celery import shared_task
from django.core.management import call_command

from backend.covid_report.polling import models


@shared_task(ignore_result=True)
def regenerate_timeslots_and_datapoints():
    for metric in models.Metric.objects.filter(is_active=True).iterator():
        metric.generate_timeslots_and_datapoints()


@shared_task(ignore_result=True)
def copy_last_value_if_slot_was_missed():
    call_command('copy_last_value_if_slot_was_missed')
