from datetime import timedelta

from django.db.models import Q
from django.db.models import QuerySet
from django.utils import timezone


class DatapointQuerySet(QuerySet):
    def unsubmitted(self):
        return self.filter(submission_datetime__isnull=True)

    def submitted(self):
        return self.filter(submission_datetime__isnull=False)

    def before(self, timestamp):
        return self.filter(timeslot__deadline__lt=timestamp)

    def after(self, timestamp):
        return self.filter(timeslot__deadline__gte=timestamp)

    def get_upcoming(self, timestamp):
        return self.after(timestamp).first()

    def compulsory(self):
        return self.filter(timeslot__metric__is_optional=False)

    def last_optional(self, metric):
        obj = self.submitted().filter(
            timeslot__metric__is_optional=True,
            timeslot__metric=metric,
        ).order_by('-timeslot__deadline').first()

        if not obj:
            obj = self.unsubmitted().filter(
                timeslot__metric__is_optional=True,
                timeslot__metric=metric,
            ).order_by('-timeslot__deadline').first()

        return obj

    def optional(self, metric, last_datapoint):
        if last_datapoint:
            last_deadline = last_datapoint.timeslot.deadline
        else:
            last_deadline = timezone.now()

        return self.filter(
            timeslot__metric__is_optional=True,
            timeslot__metric=metric,
            timeslot__deadline__gte=last_deadline,
        ).filter(
            Q(numeric_value__isnull=True) & Q(status_value__isnull=True)
            & (
                Q(text_value__isnull=True) | Q(text_value__exact='')
            )
        ).order_by('-timeslot__deadline')[:1]

    def last_2_weeks(self) -> QuerySet:
        two_weeks_ago = timezone.now() - timedelta(days=14)
        return self.filter(timeslot__deadline__gte=two_weeks_ago)

    def answered(self) -> QuerySet:
        return self.filter(
            Q(numeric_value__isnull=False) | Q(status_value__isnull=False) | (Q(text_value__isnull=False))
        )
