from django.db import migrations
from django.conf import settings


def add_new_frequency(apps, schema_editor):
    PollingFrequency = apps.get_model("polling", "PollingFrequency")
    db_alias = schema_editor.connection.alias

    PollingFrequency.objects.using(db_alias).get_or_create(
        identifier="once-a-day",
        defaults={
            "name": "Once per day (08:00)",
            "schedule_str": (
                f"DTSTART;TZID={settings.TIME_ZONE}:20200301T000000\n"
                f"RRULE:FREQ=HOURLY;INTERVAL=1;BYHOUR=8"
            ),
        },
    )


class Migration(migrations.Migration):

    dependencies = [
        ("polling", "0013_auto_20200319_2246"),
    ]

    operations = [
        migrations.RunPython(add_new_frequency, migrations.RunPython.noop),
    ]
