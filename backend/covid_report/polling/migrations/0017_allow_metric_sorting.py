
from django.db import migrations, models


def populate_order_value(apps, _):
    Metric = apps.get_model("polling", "Metric")

    order = 0
    for metric in Metric.objects.all():
        order += 1
        metric.order = order
        metric.save()


class Migration(migrations.Migration):

    dependencies = [
        ('polling', '0016_fix_length_limit_issue'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='metric',
            options={'ordering': ['order'], 'verbose_name': 'question'},
        ),
        migrations.AddField(
            model_name='metric',
            name='order',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.RunPython(populate_order_value),
    ]
