from django.urls import path

from backend.auth.views import AcceptInvite


app_name = "backend_auth"


urlpatterns = [
    path(
        "accept-invitation/<url_token>/",
        AcceptInvite.as_view(),
        name="accept_invitation",
    )
]
