{% load i18n %}{% blocktrans trimmed context 'auth-mail' %}Corona reporting for the cantonal crisis management team in Basel-Landschaft{% endblocktrans %}
{% blocktrans trimmed context 'auth-mail' %}We need your help!{% endblocktrans %}

{% blocktrans trimmed context 'auth-mail' %}Welcome to the reporting system of the cantonal crisis management team in the canton of Basel-Landschaft. This system is a quick way to pass updates to the KKS and also to send short comments.{% endblocktrans %}

{% trans "Please finish your registration here:" context 'auth-mail' %} {{ base_url }}{{ invitation.get_url }}

{% blocktrans trimmed context 'auth-mail' %}After registering, you will find further information on the data that we regularly request from you. We would like to point out that we absolutely need this information to manage the situation in the canton of Basel-Landschaft. In order to make the process as easy as possible for you, the application is optimized for mobile.{% endblocktrans %}

{% blocktrans trimmed context 'auth-mail' %}Any questions? Please do not hesitate to contact us via email {{ settings.DEFAULT_REPLY_TO_EMAIL }}{% endblocktrans %}

{% trans "Best regards," context 'auth-mail' %}
{% trans "Cantonal crisis team Basel-Landschaft" context 'auth-mail' %}
