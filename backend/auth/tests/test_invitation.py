from typing import List

from bs4 import BeautifulSoup
from django.core import mail
from django.test import Client
from django.test import TestCase
from django.test import override_settings
from django.urls import reverse

from backend.auth.models import Invitation
from backend.auth.models import Organization
from backend.auth.models import OrganizationType
from backend.auth.models import User


@override_settings(LANGUAGE_CODE='en')
class InvitationTest(TestCase):
    user_email = "victor@what.digital"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.organization: Organization = Organization.objects.create(
            name="Bern"
        )
        cls.organization_2: Organization = Organization.objects.create(
            name="Zurich"
        )
        cls.organization_type: OrganizationType = OrganizationType.objects.create(
            name="Type Zurich",
        )
        cls.organization_type.organizations.add(cls.organization_2)
        cls.organizations = Organization.objects.filter(pk=cls.organization.pk)
        cls.client_admin = cls._login_as_admin()
        cls.invitation_add_url = reverse("admin:backend_auth_invitation_add")

    def test_invitation_flow_1(self):
        self._test_invitation_configuration(
            fields_to_test=dict(
                first_name="Victor",
                last_name="Yunenko",
                role="Engineer",
                is_can_report=False,
                is_can_view=False,
                is_reviewer=True,
                phone="+44 058 384 84 34",
            )
        )

    def test_invitation_flow_2(self):
        self._test_invitation_configuration(
            fields_to_test=dict(
                last_name="Yunenko", is_can_report=True, is_can_view=False,
            )
        )

    def test_invitation_flow_for_superuser(self):
        self._test_invitation_configuration(
            fields_to_test=dict(
                last_name="Yunenko", is_superuser=True,
            )
        )

    def test_invitation_flow_3(self):
        self._test_invitation_configuration(
            fields_to_test=dict(
                role="Engineer", is_can_view=True, is_can_report=False,
            )
        )

    def test_invitation_flow_4(self):
        self._test_invitation_configuration(
            fields_to_test=dict(is_can_report=True, is_can_view=True,)
        )

    def test_invitation_flow_empty(self):
        self._test_invitation_configuration(fields_to_test={})

    def test_invitation_flow_with_reminder(self):
        invitation = Invitation.objects.create(email=self.user_email)
        invitation.organizations.add(self.organization)
        self._test_invitation_configuration(fields_to_test={})

    def test_email_for_reporter_1(self):
        self._assert_user_gets_reporter_email(
            form_data=dict(is_can_report=True)
        )

    def test_email_for_reporter_2(self):
        self._assert_user_gets_reporter_email(
            form_data=dict(is_can_report=True, is_can_view=True)
        )

    def test_email_for_viewer(self):
        self._assert_user_gets_viewer_email(form_data=dict(is_can_view=True))

    def _assert_user_gets_viewer_email(self, form_data: dict):
        self._assert_user_gets_email(
            form_data,
            strings_in_email=[
                "Welcome to the reporting system of the cantonal crisis team",
                "Always up-to-date information",
            ],
        )

    def _assert_user_gets_reporter_email(self, form_data: dict):
        self._assert_user_gets_email(
            form_data,
            strings_in_email=[
                "Welcome to the reporting system of the cantonal crisis management team",
                "We need your help!",
            ],
        )

    def _assert_user_gets_email(
        self, form_data: dict, strings_in_email: List[str]
    ):
        self.client_admin.post(
            self.invitation_add_url,
            data=dict(
                email=self.user_email,
                organizations=[self.organization.pk],
                **form_data,
            ),
            follow=True,
        )
        self.assertEquals(len(mail.outbox), 1)
        email = mail.outbox[0]
        # for str_in_email in strings_in_email:
        #     self.assertIn(str_in_email, email.body)
        #     self.assertIn(str_in_email, email.alternatives[0][0])

    def _test_invitation_configuration(self, fields_to_test: dict):
        response = self.client_admin.post(
            self.invitation_add_url,
            data=dict(
                email=self.user_email,
                organizations=[self.organization.pk],
                **fields_to_test,
            ),
            follow=True,
        )
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(mail.outbox), 1)

        bs = BeautifulSoup(response.rendered_content, "html.parser")
        invitation_created_pk = bs.find(
            "input", {"name": "_selected_action"}
        ).attrs["value"]
        invitation_created: Invitation = Invitation.objects.get(
            pk=invitation_created_pk
        )
        self.assertEquals(invitation_created.is_accepted, False)
        invitation_url = invitation_created.get_url()

        response = self.client.get(invitation_url, follow=True)
        self.assertContains(response, self.user_email)
        self.assertEquals(response.status_code, 200)
        signup_url: str = "/" + response.resolver_match.route
        response = self.client.post(
            signup_url,
            data=dict(email=self.user_email, password1=self.user_email,),
            follow=True,
        )
        self.assertEquals(response.status_code, 200)
        user_created = User.objects.get(email=self.user_email)
        for field_name, field_value_expected in fields_to_test.items():
            self.assertEquals(
                getattr(user_created, field_name), field_value_expected
            )
        self.assertEqual(
            list(user_created.organizations.all()),
            list(self.organizations),
        )


        Invitation.objects.filter(
            email=self.user_email, is_accepted=True
        ).order_by("created_at").last()

        self.client.logout()
        response = self.client.get(invitation_url, follow=True)
        self.assertContains(
            response, "This invitation has been accepted already"
        )

    @classmethod
    def _login_as_admin(cls) -> Client:
        admin_email = "test@what.digital"
        if User.objects.filter(email=admin_email).exists():
            user = User.objects.get(email=admin_email)
        else:
            user = User.objects.create_superuser(
                email=admin_email, password=admin_email
            )
        client = Client()
        client.force_login(user)
        return client
