from django.conf import settings
from django.utils.translation import pgettext
from djangocms_helpers.utils.send_email import send_email

from backend.auth.models import Invitation


def send_invitation_email(invitation: Invitation):
    if invitation.is_can_report:
        subject = pgettext(
            "auth-mail", "KKS Basel-Landschaft: We need your help"
        )
        template_path_without_extension = (
            "account/email/invitation-for-reporter"
        )
    else:
        subject = pgettext(
            "auth-mail", "KKS Basel-Landschaft: Always up to date"
        )
        template_path_without_extension = (
            "account/email/invitation-for-viewer"
        )

    send_email(
        email_destination=invitation.email,
        email_subject=subject,
        email_reply_to=settings.DEFAULT_REPLY_TO_EMAIL,
        template_path_without_extension=template_path_without_extension,
        template_context={"invitation": invitation},
    )
