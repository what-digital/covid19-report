from django import forms
from django.forms import Form
from django.http import HttpRequest
from django.utils.translation import pgettext

from backend.auth.models import Invitation
from backend.auth.models import Organization
from backend.auth.models import OrganizationType
from backend.auth.models import User
from backend.auth.services import send_invitation_email


class CovidSignupForm(Form):
    def signup(self, request: HttpRequest, user: User):
        orgs = Organization.objects.filter(pk__in=request.session["organizations_pks"])
        for org in orgs:
            user.organizations.add(org)
        org_types = OrganizationType.objects.filter(pk__in=request.session["organization_types_pks"])
        for org_type in org_types:
            user.organization_types.add(org_type)
        user.first_name = request.session.get("first_name", "")
        user.last_name = request.session.get("last_name", "")
        user.role = request.session.get("role", "")
        user.phone = request.session.get("phone", "")
        user.is_can_view = request.session.get("is_can_view", False)
        user.is_can_report = request.session.get("is_can_report", False)
        user.is_superuser = request.session.get("is_superuser", False)
        user.is_reviewer = request.session.get("is_reviewer", False)
        if user.is_reviewer:
            user.is_staff = True
        user.save()

        invitation = (
            Invitation.objects.filter(email=user.email, is_accepted=False)
            .order_by("created_at")
            .last()
        )
        invitation.is_accepted = True
        invitation.save()


class InvitationAdminAddForm(forms.ModelForm):
    def save(self, *args, **kwargs) -> Invitation:
        instance: Invitation = super().save(*args, **kwargs)
        send_invitation_email(instance)        
        return instance

    def clean_email(self) -> str:
        email = self.cleaned_data["email"]
        already_invited_error = forms.ValidationError(
            pgettext("auth", "The user has already accepted the invitation")
        )
        if Invitation.objects.filter(email=email, is_accepted=True).exists():
            raise already_invited_error
        elif User.objects.filter(email=email).exists():
            raise already_invited_error
        else:
            return email

    class Meta:
        model = Invitation
        fields = [
            "email",
            "first_name",
            "last_name",
            "role",
            "phone",
            "is_can_report",
            "is_can_view",
            "is_superuser",
            "is_reviewer",
            "organizations",
            "organization_types",
        ]
