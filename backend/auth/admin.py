from typing import List

from adminutils import options
from cuser.admin import UserAdmin
from cuser.forms import UserChangeForm
from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.contrib.admin import SimpleListFilter
from django.db.models import Q
from django.db.models import QuerySet
from django.forms import Form
from django.http import HttpRequest
from django.http import HttpResponse
from django.utils.translation import ugettext_lazy as _
from hijack_admin.admin import HijackUserAdminMixin
from import_export import fields
from import_export.admin import ImportMixin
from import_export.resources import ModelResource
from import_export.tmp_storages import CacheStorage
from import_export.widgets import ManyToManyWidget

from backend.auth import models
from backend.auth.forms import InvitationAdminAddForm
from backend.auth.models import Invitation
from backend.auth.models import Organization
from backend.auth.models import OrganizationType
from backend.auth.models import User
from backend.auth.services import send_invitation_email


class OrganizationTypeFilter(admin.SimpleListFilter):
    title = _("Organization Type")
    parameter_name = 'organization_type'

    def lookups(self, request, model_admin):
        if request.user.is_reviewer:
            orgs = OrganizationType.objects.filter(
                pk__in=request.user.organization_types.all()
            ).distinct()
        else:
            orgs = OrganizationType.objects.all()
        return ((org.id, org.name) for org in orgs)

    def queryset(self, request, queryset):
        org_pk = self.value()
        return queryset.filter(organization_types__in=[org_pk]) if org_pk else queryset


class OrganizationFilter(SimpleListFilter):
    title = _("Organization")
    parameter_name = 'organization'

    def lookups(self, request, model_admin):
        if request.user.is_reviewer:
            orgs = Organization.objects.filter(
                pk__in=request.user.organizations.all()
            ).distinct()
        else:
            orgs = Organization.objects.all()
        return ((org.id, org.name) for org in orgs)

    def queryset(self, request, queryset):
        org_pk = self.value()
        return queryset.filter(organizations__in=[org_pk]) if org_pk else queryset


class CovidUserChangeForm(UserChangeForm):

    def clean(self) -> dict:
        if self.cleaned_data['is_reviewer']:
            self.cleaned_data['is_staff'] = True
        return super().clean()


@admin.register(models.User)
class CovidUserAdmin(UserAdmin, HijackUserAdminMixin):
    form = CovidUserChangeForm
    change_form_template = "admin/auth/user_changeform.html"

    list_filter = [
        "is_reviewer",
        OrganizationTypeFilter,
        OrganizationFilter,
        "is_can_view",
        "is_can_report",
        "is_staff",
        "is_superuser",
        "is_active",
    ]
    filter_horizontal = [
        'organizations',
        'organization_types',
        'groups',
        'user_permissions',
    ]

    def get_list_display(self, request: HttpRequest) -> List[str]:
        list_fields = [
            "email",
            "role",
            "first_name",
            "last_name",
            "organizations_list",
            "is_can_view",
            "is_can_report",
            "is_superuser",
        ]
        if request.user.is_superuser:
            list_fields.append('hijack_field')
        return list_fields

    @options(desc=_("organisations"))
    def organizations_list(self, obj: User) -> str:
        return ", ".join(
            obj.get_accessible_organizations().values_list("name", flat=True)
        )

    def get_fieldsets(
        self, request: HttpRequest, obj: User = None
    ) -> List[dict]:
        is_add_view = obj is None
        if is_add_view:
            return super().get_fieldsets(request, obj)
        else:
            fieldset_base = [
                (None, {"fields": [
                    "email",
                    "password",
                ]}),
                (
                    _("Personal info"),
                    {"fields": [
                        "first_name",
                        "last_name",
                        "role",
                        "phone",
                        "organizations",
                        "organization_types",
                        "groups",
                        "user_permissions",
                    ]},
                ),
            ]
            if request.user.is_superuser:
                return [
                    *fieldset_base,
                    (
                        _("Permissions"),
                        {
                            "fields": [
                                "is_reviewer",
                                "is_can_view",
                                "is_can_report",
                                "is_staff",
                                "is_superuser",
                                "is_active",
                            ]
                        },
                    ),
                ]
            return [
                *fieldset_base,
                (
                    _("Permissions"),
                    {
                        "fields": [
                            "is_reviewer",
                            "is_can_view",
                            "is_can_report",
                            "is_active",
                            "is_staff",
                        ]
                    },
                ),
            ]

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None) -> HttpResponse:
        context['is_has_reviewer_perms'] = (
            request.user.is_reviewer and
            self._is_can_manage(reviewer=request.user, user=obj)
        )
        return super().render_change_form(request, context, add, change, form_url, obj)

    def get_readonly_fields(self, request: HttpRequest, obj: User = None) -> List[str]:
        field_all = list(set(
            [field.name for field in self.opts.local_fields] +
            [field.name for field in self.opts.local_many_to_many]
        ))
        if request.user.is_reviewer and self._is_can_manage(reviewer=request.user, user=obj):
            field_all.remove('password')
            return field_all
        elif request.user.is_reviewer:
            return field_all
        else:
            return super().get_readonly_fields(request, obj)

    def has_change_permission(self, request, obj: User = None) -> bool:
        if not obj:
            return super().has_change_permission(request, obj)
        if (
            self._is_has_reviewer_perms(request.user) and
            self._is_can_manage(reviewer=request.user, user=obj)
        ):
            return True
        elif self._is_has_reviewer_perms(request.user):
            return False
        else:
            return super().has_change_permission(request, obj)

    def has_view_permission(self, request, obj=None):
        if (
            self._is_has_reviewer_perms(request.user) and
            self._is_can_manage(reviewer=request.user, user=obj)
        ):
            return True
        else:
            return super().has_view_permission(request, obj)

    def has_module_permission(self, request) -> bool:
        if request.user.is_reviewer:
            return True
        else:
            return super().has_module_permission(request)

    def get_queryset(self, request: HttpRequest) -> QuerySet:
        if self._is_has_reviewer_perms(request.user):
            return (
                User.objects
                    .filter(
                        Q(organization_types__pk__in=request.user.organization_types.all()) |
                        Q(organizations__types__pk__in=request.user.organization_types.all()) |
                        Q(organizations__pk__in=request.user.organizations.all()),
                    )
                    .filter(
                        is_superuser=False,
                    )
                    .distinct()
            )
        else:
            return super().get_queryset(request)

    def _is_can_manage(self, reviewer: User, user: User = None) -> bool:
        is_list_view = not user
        if is_list_view:
            return True
        return (
            User.objects
                .filter(
                    Q(organization_types__pk__in=reviewer.organization_types.all()) |
                    Q(organizations__types__pk__in=reviewer.organization_types.all())|
                    Q(organizations__pk__in=reviewer.organizations.all()),
                )
                .filter(pk=user.pk, is_superuser=False)
                .exists()
        )

    def _is_has_reviewer_perms(self, user: User) -> bool:
        return user.is_reviewer and not user.is_superuser


@admin.register(models.OrganizationType)
class OrganizationTypeAdmin(admin.ModelAdmin):
    pass


class OrganizationResource(ModelResource):
    types = fields.Field(
        column_name='types',
        attribute='types',
        widget=ManyToManyWidget(OrganizationType, field='name'),
    )

    class Meta:
        model = Organization
        import_id_fields = ['name']
        fields = [
            "name",
            "types",
            "is_enable_reminder_emails",
        ]
        export_order = fields
        skip_unchanged = True


@admin.register(models.Organization)
class OrganizationAdmin(ImportMixin, admin.ModelAdmin):
    resource_class = OrganizationResource
    tmp_storage_class = CacheStorage
    list_display = (
        "name",
        "is_enable_reminder_emails",
        "is_automatically_copy_last_answer",
        "users_column",
        "types_column",
    )
    list_filter = (
        "is_enable_reminder_emails",
        "is_automatically_copy_last_answer",
        "types",
    )
    search_fields = [
        "name",
        "users__email",
    ]

    filter_horizontal = [
        "types",
    ]
    readonly_fields = ["users_field"]

    fieldsets = [
        (None, {
            "fields": [
                "name",
                "users_field",
                "types",
                "is_enable_reminder_emails",
                "is_automatically_copy_last_answer",
            ]
        }),
    ]

    change_list_template = "admin/auth/organization_list.html"

    def users_column(self, obj: models.Organization) -> str:
        emails: str = ", ".join([user.email for user in obj.users.all()])
        return emails[:100]

    users_column.short_description = "users"

    def types_column(self, obj: models.Organization) -> str:
        types: str = ", ".join([type.name for type in obj.types.all()])
        return types[:100]

    types_column.short_description = "types"

    def users_field(self, obj: models.Organization) -> str:
        return ",\n".join([user.email for user in obj.users.all()])

    users_field.short_description = "users"


class InvitationResource(ModelResource):
    organizations = fields.Field(
        column_name='organizations',
        attribute='organizations',
        widget=ManyToManyWidget(Organization, field='name'),
    )
    organization_types = fields.Field(
        column_name='organization_types',
        attribute='organization_types',
        widget=ManyToManyWidget(OrganizationType, field='name'),
    )

    class Meta:
        model = Invitation
        import_id_fields = ['email']
        fields = [
            "email",
            "organizations",
            "organization_types",
            "first_name",
            "last_name",
            "role",
            "phone",
            "is_can_report",
            "is_can_view",
            "is_superuser",
        ]
        export_order = fields
    
    def after_save_instance(
        self, instance: Invitation, using_transactions: bool, dry_run: bool,
    ):
        super().after_save_instance(instance, using_transactions, dry_run)
        if dry_run is False:
            send_invitation_email(instance)


@admin.register(models.Invitation)
class InvitationAdmin(ImportMixin, ModelAdmin):
    resource_class = InvitationResource
    tmp_storage_class = CacheStorage
    list_display = [
        "email",
        "is_accepted",
        "first_name",
        "last_name",
        "role",
        "phone",
        "is_can_report",
        "is_can_view",
        "is_superuser",
        "created_at",
    ]
    list_filter = [
        "organizations",
        "organization_types",
        "is_accepted",
        "created_at",
        "is_can_view",
        "is_can_report",
        "is_superuser",
    ]
    search_fields = [
        "email",
        "first_name",
        "last_name",
    ]
    filter_horizontal = [
        'organizations',
        'organization_types',
    ]

    change_form_template = "admin/auth/invitation.html"

    def get_form(
        self, request: HttpRequest, obj: Invitation = None, **kwargs
    ) -> Form:
        is_add_view = not obj
        if is_add_view:
            kwargs["form"] = InvitationAdminAddForm
        return super().get_form(request, obj, **kwargs)
