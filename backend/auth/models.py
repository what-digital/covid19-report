import uuid

from cuser.models import AbstractCUser
from django.db import models
from django.db.models import Q
from django.db.models import QuerySet
from django.urls import reverse
from django.utils import timezone
from django.utils.text import slugify
from django.utils.translation import pgettext_lazy


class OrganizationType(models.Model):
    name = models.CharField(max_length=512, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Organization(models.Model):
    name = models.CharField(max_length=512, unique=True)
    slug = models.SlugField(max_length=512, unique=True, blank=True)
    types = models.ManyToManyField(OrganizationType, related_name='organizations')
    is_enable_reminder_emails = models.BooleanField(
        default=True,
        verbose_name=pgettext_lazy("auth", "Enable email reminders"),
    )
    is_automatically_copy_last_answer = models.BooleanField(
        default=False,
        verbose_name=pgettext_lazy('auth', "Autofill"),
        help_text=pgettext_lazy('auth', "Automatically copy the last value if the slot was missed"),
    )

    def __str__(self):
        return self.name

    def get_url(self) -> str:
        return reverse(
            'polling:pending_for_org',
            kwargs={'org_slug': self.slug},
        )

    def get_pending_datapoints_count(self) -> int:
        from backend.covid_report.polling.models import Datapoint
        from backend.covid_report.polling.models import Metric

        org_datapoints = Datapoint.objects.filter(organization=self)
        unsubmitted = (
            org_datapoints
                .filter(timeslot__metric__is_optional=False)
                .before(timezone.now())
                .unsubmitted()
        )
        unsubmitted_count = unsubmitted.count()
        metric_ids = org_datapoints.unsubmitted()\
            .values_list("timeslot__metric_id", flat=True)\
            .distinct()
        metrics = Metric.objects.filter(pk__in=metric_ids)

        for metric in metrics:
            future_datapoints = Datapoint.objects \
                .filter(organization=self, timeslot__metric=metric) \
                .after(timezone.now())

            for datapoint in future_datapoints.all():
                if datapoint.is_upcoming() and datapoint.value is None:
                    unsubmitted_count += 1

        return unsubmitted_count

    def is_has_overdue_datapoints(self) -> bool:
        from backend.covid_report.polling.models import Datapoint
        return (
            Datapoint.objects.filter(organization=self)
                .filter(timeslot__metric__is_optional=False)
                .before(timezone.now())
                .unsubmitted()
                .exists()
        )

    def get_overdue_datapoints_count(self) -> int:
        from backend.covid_report.polling.models import Datapoint
        return (
            Datapoint.objects.filter(organization=self)
                .filter(timeslot__metric__is_optional=False)
                .before(timezone.now())
                .unsubmitted()
                .count()
        )

    def get_last_answered_datapoint(self, datapoint: 'Datapoint') -> 'Datapoint':
        from backend.covid_report.polling.models import Datapoint
        return (
            Datapoint.objects
                .filter(
                    organization=self,
                    timeslot__metric_id=datapoint.timeslot.metric_id,
                    timeslot__deadline__lte=datapoint.timeslot.deadline
            )
                .submitted()
                .answered()
                .before(timezone.now())
                .order_by('-timeslot__deadline')
                .only('numeric_value', 'status_value', 'text_value', 'id')
                .first()
        )

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)

    class Meta:
        ordering = ['name']


class User(AbstractCUser):
    """
    The base of this class cuser.AbstractCUser sets the email as username.
    """

    #: This field is required by cms.PageUserAdmin, because that class
    #: violets its own bloody documentation. Otherwise this field is useless.
    username = models.CharField(
        max_length=128,
        blank=True,
        help_text='Serves only cosmetic purpose, can be ignored',
    )

    organizations = models.ManyToManyField(
        Organization,
        verbose_name=pgettext_lazy("auth", "Organizations"),
        related_name='users',
        blank=True,
    )

    organization_types = models.ManyToManyField(
        OrganizationType,
        verbose_name=pgettext_lazy("auth", "Organization types"),
        related_name='users',
        blank=True,
    )
    phone = models.CharField(max_length=256, blank=True)
    role = models.CharField(max_length=512, blank=True)
    is_can_report = models.BooleanField(
        default=False, verbose_name=pgettext_lazy("auth", "can post report")
    )
    is_can_view = models.BooleanField(
        default=False, verbose_name=pgettext_lazy("auth", "can view reports")
    )
    is_reviewer = models.BooleanField(default=False, verbose_name=pgettext_lazy("auth", "Reviewer"))

    class Meta(AbstractCUser.Meta):
        swappable = "AUTH_USER_MODEL"
        abstract = False

    def get_accessible_organizations(self) -> QuerySet:
        return (
            Organization.objects
                .filter(
                    Q(pk__in=self.organizations.all()) |
                    Q(types__in=self.organization_types.all())
                )
                .distinct()
                .order_by('name')
        )


class Invitation(models.Model):
    url_token = models.UUIDField(
        default=uuid.uuid4, editable=False, unique=True
    )
    email = models.EmailField()

    first_name = models.CharField(
        max_length=512, blank=True, verbose_name=pgettext_lazy("auth", "first name")
    )
    last_name = models.CharField(
        max_length=512, blank=True, verbose_name=pgettext_lazy("auth", "last name")
    )
    role = models.CharField(
        max_length=512, blank=True, verbose_name=pgettext_lazy("auth", "role")
    )
    phone = models.CharField(max_length=256, blank=True)
    organizations = models.ManyToManyField(
        Organization,
        verbose_name=pgettext_lazy("auth", "Organization"),
        related_name='invitations',
        blank=True,
    )
    organization_types = models.ManyToManyField(
        OrganizationType,
        verbose_name=pgettext_lazy("auth", "Organization types"),
        related_name='invitations',
        blank=True,
    )
    is_can_report = models.BooleanField(
        default=False, verbose_name=pgettext_lazy("auth", "can post report")
    )
    is_can_view = models.BooleanField(
        default=False, verbose_name=pgettext_lazy("auth", "can view reports")
    )
    is_superuser = models.BooleanField(
        default=False, verbose_name=pgettext_lazy("auth", "superuser")
    )
    is_reviewer = models.BooleanField(
        default=False,
        verbose_name=pgettext_lazy("auth", "Reviewer"),
        help_text=pgettext_lazy('auth', "Allows to manage answers from accessible organization types, as well as password resets.")
    )

    is_accepted = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def get_url(self) -> str:
        return reverse(
            "backend_auth:accept_invitation",
            kwargs={"url_token": self.url_token},
        )

    def get_accessible_organizations(self) -> QuerySet:
        return (
            Organization.objects
                .filter(
                    Q(pk__in=self.organizations.all()) |
                    Q(types__in=self.organization_types.all())
                )
                .distinct()
                .order_by('name')
        )

    def __str__(self):
        return self.email
