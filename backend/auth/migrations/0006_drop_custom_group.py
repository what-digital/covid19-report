# Generated by Django 2.2.11 on 2020-03-17 09:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("backend_auth", "0005_add_permissions_to_invitations"),
    ]

    operations = [
        migrations.AlterField(
            model_name="invitation",
            name="is_can_view",
            field=models.BooleanField(
                default=False, verbose_name="Can view reports"
            ),
        ),
        migrations.DeleteModel(name="ReporterGroup",),
    ]
