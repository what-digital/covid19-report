from django.db import migrations, models
from django.utils.text import slugify


def populate_org_slug_field(apps, _):
    Organization = apps.get_model("backend_auth", "Organization")
    for org in Organization.objects.all():
        org.slug = slugify(org.name)
        org.save()


def migrate_organization_fk_to_m2m(apps, _):
    User = apps.get_model("backend_auth", "User")
    for user in User.objects.all():
        if user.organization:
            user.organization.users.add(user)
            user.organization.save()


class Migration(migrations.Migration):

    dependencies = [
        ('backend_auth', '0016_add_organizations_m2m'),
    ]

    operations = [
        migrations.AddField(
            model_name='organization',
            name='slug',
            field=models.SlugField(blank=True, max_length=512),
        ),
        migrations.RunPython(populate_org_slug_field),
        migrations.RunPython(migrate_organization_fk_to_m2m),
        migrations.RemoveField(
            model_name='invitation',
            name='organization',
        ),
    ]
