# Generated by Django 2.2.11 on 2020-05-31 21:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend_auth', '0018_make_slug_field_unique'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='organization',
            options={'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='organizationtype',
            options={'ordering': ['name']},
        ),
    ]
