from django.db import migrations
from django.db import models


class Migration(migrations.Migration):

    dependencies = [
        ('backend_auth', '0020_make_organization_optional'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='organization_types',
            field=models.ManyToManyField(blank=True, related_name='users', to='backend_auth.OrganizationType', verbose_name='Organization types'),
        ),
        migrations.AlterField(
            model_name='organization',
            name='types',
            field=models.ManyToManyField(related_name='organizations', to='backend_auth.OrganizationType'),
        ),
    ]
