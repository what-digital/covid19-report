from django.contrib.auth.mixins import AccessMixin
from django.http import HttpRequest
from django.shortcuts import get_object_or_404
from django.utils.translation import pgettext

from backend.auth.models import Organization
from backend.covid_report.polling.middleware import Redirect


login_required_error = pgettext(
    'auth',
    "You must be logged in order to access this page.",
)


class OrganizationRequiredMixin(AccessMixin):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            raise Redirect("account_login", login_required_error)
        if not request.user.get_accessible_organizations().exists() and request.user.is_superuser:
            org_error = pgettext(
                'auth',
                "You need to assign an organization to your user in order to access the reports pages.",
            )
            raise Redirect("admin:backend_auth_user_change", org_error, args=[request.user.id])
        if not request.user.get_accessible_organizations().exists():
            org_error = pgettext(
                'auth',
                "Your user doesn't have access to any organization, "
                "please contact support.",
            )
            raise Redirect("account_login", org_error)
        return super().dispatch(request, *args, **kwargs)


class OrganizationAccessMixin(AccessMixin):
    def dispatch(self, request: HttpRequest, *args, **kwargs):
        org_current = get_object_or_404(Organization, slug=kwargs.get('org_slug'))
        if not request.user.get_accessible_organizations():
            raise Redirect("account_login", login_required_error)
        if org_current.pk not in request.user.get_accessible_organizations().values_list("pk", flat=True):
            org_error = pgettext(
                'auth',
                "You don't have access to this organization"
            )
            raise Redirect("polling:pending", org_error)
        return super().dispatch(request, *args, **kwargs)


class ReporterRequiredMixin(AccessMixin):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            raise Redirect("account_login", login_required_error)
        
        is_only_view_perm = (
            not request.user.is_can_report and
            not request.user.is_superuser
        )
        if is_only_view_perm:
            if request.user.is_reviewer:
                raise Redirect('admin:index')
            else:
                raise Redirect(
                    "polling:submitted-metric-list",
                    error_message=pgettext(
                        'auth',
                        "You are not able to submit reports.",
                    ),
                )

        return super().dispatch(request, *args, **kwargs)


class ViewerRequiredMixin(AccessMixin):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            raise Redirect("account_login", login_required_error)
        
        is_only_report_perm = (
            not request.user.is_can_view and
            not (request.user.is_superuser or request.user.is_staff)
        )
        if is_only_report_perm:
            raise Redirect(
                "polling:pending",
                error_message=pgettext(
                    'auth',
                    "You are not allowed to view the reports history.",
                ),
            )

        return super().dispatch(request, *args, **kwargs)
