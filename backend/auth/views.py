from typing import Optional

from allauth.account.adapter import DefaultAccountAdapter
from django.conf import settings
from django.contrib import messages
from django.db.models import QuerySet
from django.http import HttpResponse
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.translation import pgettext
from django.views import View
from django.views.generic.detail import SingleObjectMixin

from backend.auth.models import Invitation


class AcceptInvite(SingleObjectMixin, View):
    def get(self, *args, **kwargs) -> HttpResponse:
        return self.post(*args, **kwargs)

    def post(self, *args, **kwargs) -> HttpResponse:
        invitation: Invitation = self.get_object()
        # noinspection PyAttributeOutsideInit
        self.object = invitation

        if not invitation:
            messages.error(
                self.request, pgettext("auth", "Invalid invitation url.")
            )
            return redirect(settings.LOGIN_URL)

        if invitation.is_accepted:
            messages.error(
                self.request,
                pgettext("auth", "This invitation has been accepted already."),
            )
            return redirect(settings.LOGIN_URL)

        self._populate_request_session_for_signup_form(invitation)

        return redirect(reverse("account_signup"))

    def get_object(self, queryset: QuerySet = None) -> Optional[Invitation]:
        if queryset is None:
            queryset = self.get_queryset()
        try:
            return queryset.get(url_token=self.kwargs["url_token"])
        except Invitation.DoesNotExist:
            return None

    def get_queryset(self) -> QuerySet:
        return Invitation.objects.all()

    def _populate_request_session_for_signup_form(
        self, invitation: Invitation
    ):
        DefaultAccountAdapter().stash_verified_email(
            self.request, invitation.email,
        )
        self.request.session["organizations_pks"] = [
            org.pk for org in invitation.organizations.all()
        ]
        self.request.session["organization_types_pks"] = [
            org_type.pk for org_type in invitation.organization_types.all()
        ]
        self.request.session["first_name"] = invitation.first_name
        self.request.session["last_name"] = invitation.last_name
        self.request.session["is_can_report"] = invitation.is_can_report
        self.request.session["is_can_view"] = invitation.is_can_view
        self.request.session["is_superuser"] = invitation.is_superuser
        if invitation.is_reviewer:
            self.request.session["is_reviewer"] = invitation.is_reviewer
            self.request.session["is_staff"] = True
        self.request.session["role"] = invitation.role
        self.request.session["phone"] = invitation.phone
