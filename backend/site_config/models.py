from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import pgettext
from solo.models import SingletonModel

User = get_user_model()


class SiteConfig(SingletonModel):
    system_user = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        null=True,
        help_text=pgettext(
            'site-config',
            'This user is going to be assigned as reporter to the automatically generated answers.'
        )
    )

    def __str__(self) -> str:
        return 'Site Config'
