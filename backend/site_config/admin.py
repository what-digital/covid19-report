from django.contrib import admin
from solo.admin import SingletonModelAdmin

from backend.site_config.models import SiteConfig


@admin.register(SiteConfig)
class SiteConfigAdmin(SingletonModelAdmin):
    pass
