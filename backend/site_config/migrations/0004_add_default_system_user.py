from django.db import migrations

from backend.auth.models import User
from backend.site_config.models import SiteConfig


def add_default_system_user(apps, schema_editor):
    site_config = SiteConfig.get_solo()
    if not site_config.system_user:
        site_config.system_user = User.objects.create(email='system@bl.coreport.bl')
        site_config.save()


class Migration(migrations.Migration):

    dependencies = [
        ("site_config", "0003_delete_blank_on_system_user_field"),
    ]

    operations = [
        migrations.RunPython(add_default_system_user, migrations.RunPython.noop),
    ]
