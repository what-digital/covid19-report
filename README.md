CoReport Introduction
=====================

CoReport is an open-source tool that enables an organization to collect structured status reports from other organizations at set intervals.

It is currently being deployed by the Swiss canton of Baselland on hosting sponsored by Divio. [Divio](https://www.divio.com/) is also donating further hosting of the tool for other organizations.

The development of CoReport was initiated by [what.](https://what.digital) in collaboration with [Divio](https://www.divio.com/). The first iteration of the tool was developed for the Swiss canton of Baselland.

CoReport is provided as a ready-to-run Dockerised web application and is under active development.


Using and contributing to CoReport
-----------------------------------

Any organisation or individual is invited to adopt and adapt this software for its own purposes.

We also welcome contributions; please see [the documentation](https://docs.coreport.ch) to
get started.

For assistance with deployment, please contact [Divio](mailto:letstalk@divio.com).

For all other queries, please contact <contact@coreport.ch>.


Key resources
-------------

* [source code](https://gitlab.com/what-digital/covid19-report)
* [documentation](https://divio-covid-report.readthedocs-hosted.com)
  * [Get started - set up a CoReport project for development and deployment](https://divio-covid-report.readthedocs-hosted.com/en/latest/get-started/)
  * [How-to guides for developers](https://divio-covid-report.readthedocs-hosted.com/en/latest/how-to/)
  * [Reference](https://divio-covid-report.readthedocs-hosted.com/en/latest/reference/)
  * [Explanation - further background information about the project](https://divio-covid-report.readthedocs-hosted.com/en/latest/background/)


Contributors
------------

Thanks to our [contributors](https://divio-covid-report.readthedocs-hosted.com/en/latest/reference/#contributors).
Please join them!
