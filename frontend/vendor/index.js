import * as Sentry from '@sentry/browser';


// this will let us have a global jQuery object
window.$ = window.jQuery = require("jquery");


// bootstrap4
require('bootstrap');
require('noty/lib/noty.css');

require('bootstrap-select/dist/js/bootstrap-select');
require('bootstrap-select/dist/js/i18n/defaults-de_DE');
require('bootstrap-select/dist/css/bootstrap-select.css');

// Icons
require('@fortawesome/fontawesome-free/scss/fontawesome.scss');
require('@fortawesome/fontawesome-free/scss/brands.scss');
require('@fortawesome/fontawesome-free/scss/solid.scss');
require('@fortawesome/fontawesome-free/scss/regular.scss');


Sentry.init({
    dsn: 'https://97179e24582044d6b139e6fc1b6ec8ab@sentry.io/5180197',
    environment: DJANGO.env,
    blacklistUrls: [
        '127.0.0.1',
        'localhost',
    ],
});

// DEMO tools that frontend engineers like
// if you want to install one of them cd to the frontend directory and: npm i <name> --save

// window.enquire = require('enquire.js');
// window.FastClick = require('fastclick');
// require('jquery-debouncedresize');
// require('imagesloaded');
// require('jquery-match-height');
// window.Cookies = require('js-cookie');
// window.svg4everybody = require('svg4everybody');
// window.objectFitImages = require('object-fit-images');
// window.autosize = require('autosize');

// require('slick-carousel/slick/slick.scss');
// require('slick-carousel/slick/slick-theme.scss');
// require('slick-carousel');


// Modernizr global :)
// window.Modernizr = require('modernizr');

// window.moment = require('moment');
// require('moment-timezone');
