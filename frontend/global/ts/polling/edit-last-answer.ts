import {showCommentBox} from 'global/ts/polling/comment-checkbox';

export function initEditableActions() {
    const editableForms = document.querySelectorAll(`[data-is-filled=true]`);
    for (const form of editableForms as any) {
        initEditableForm(form)
    }
}


const selectors = {
    editBtn: `.edit-btn`,
    commentCheckboxLabel: `.form-check-label`,
};


export function initEditableForm(form: HTMLFormElement) {
    disableEditingMode(form);
    initEditableCommentField(form);

    form.querySelector(selectors.editBtn).addEventListener(`click`, (event) => {
        event.preventDefault();
        enableEditingMode(form);
    });
}


function enableEditingMode(form: HTMLFormElement) {
    setDisabledStatusOfFormTo(form, false);

    const editBtn = form.querySelector(selectors.editBtn) as HTMLElement;
    editBtn.dataset.isVisible = String(false);
}


function disableEditingMode(form: HTMLFormElement) {
    setDisabledStatusOfFormTo(form, true);
    
    const editBtn = form.querySelector(selectors.editBtn) as HTMLElement;
    editBtn.dataset.isVisible = String(true);
}


function initEditableCommentField(form: HTMLFormElement) {
    const commentCheckbox = form.querySelector(selectors.commentCheckboxLabel) as HTMLInputElement;
    const isCommentFieldPresent = commentCheckbox !== null;
    if (isCommentFieldPresent) {
        commentCheckbox.checked = true;
        const commentInput = form.querySelector(`[name=comment]`) as HTMLInputElement;
        const isCommentPresent = Boolean(commentInput.value !== ``);
        if (isCommentPresent) {
            const checkboxElem = form.querySelector(`.comment-checkbox`) as HTMLInputElement;
            checkboxElem.checked = true;
            showCommentBox(checkboxElem);
        }
    }
}


function setDisabledStatusOfFormTo(form: HTMLFormElement, status: boolean) {
    $(form).find(':input').prop('disabled', status);
    
    const commentCheckbox = form.querySelector(selectors.commentCheckboxLabel) as HTMLInputElement;
    if (commentCheckbox) {
        commentCheckbox.dataset.isDisabled = String(status);
    }
}
