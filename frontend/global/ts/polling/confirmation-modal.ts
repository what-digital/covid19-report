import {submitFormUsingAjax} from 'global/ts/polling/submit-form-using-ajax';


const selectors = {
    form: '.slot-form',
    formSubmitBtn: '.form-submit-btn',
    textInput: 'textarea[name="value"]',
    numberInput: '[type="number"][name="value"]',
    commentInput: '[name="comment"]',
    commentCheckboxInput: 'input[type="checkbox"]',
    statusInput: 'option:checked',

    modalContainer: `#datapoint-confirmation-modal`,
    modalTitle: `#modal-title`,
    modalDateElem: '#time_text',
    modalValueElem: '#value_text',
    modalCommentElem: '#comment_text',
    modalCommentContainer: '#comment-section',
    modalSubmitBtn: '#modal-submit',
};


export function confirmModalInit() {    
    $(document).on('click', selectors.formSubmitBtn, (event) => {
        const form = $(event.target).closest('form')[0];

        const isFormInvalid = form.checkValidity() === false;
        if (isFormInvalid) {
            form.reportValidity();
            return
        }
        
        configureModalContent(form);
        $(selectors.modalContainer).modal('show');
    });

    $(document).on('click', selectors.modalSubmitBtn, (e) => {
        const modalElem: HTMLDivElement = document.querySelector(selectors.modalContainer);
        const form = document.querySelector('#' + modalElem.dataset.formId) as HTMLFormElement;
        submitFormUsingAjax(form);
        $(selectors.modalContainer).modal('hide');
    });
}


function getValueToShow(form: HTMLFormElement): string {
    const numberInput: HTMLInputElement = form.querySelector(selectors.numberInput);
    const textInput: HTMLInputElement = form.querySelector(selectors.textInput);
    const statusOption: HTMLOptionElement = form.querySelector(selectors.statusInput);

    if (numberInput) {
        return numberInput.value;
    } else if (textInput) {
        return textInput.value;
    } else if (statusOption) {
        return statusOption.text;
    }
}


function configureModalContent(form: HTMLFormElement) {
    const modalTitleElem = document.querySelector(selectors.modalTitle);
    modalTitleElem.textContent = form.dataset.name;

    $(selectors.modalDateElem).html($(form).data('time'));

    let valueToShow: string = getValueToShow(form);
    $(selectors.modalValueElem).html(valueToShow);

    const commentCheckboxInput = form.querySelector(selectors.commentCheckboxInput) as HTMLInputElement;
    const commentInput = form.querySelector(selectors.commentInput) as HTMLInputElement;
    let isCommentPresent = false;
    if (commentCheckboxInput) {
        isCommentPresent = commentCheckboxInput.value && commentCheckboxInput.checked;
    }
    if (isCommentPresent) {
        if (commentInput.value !== '') {
            $(selectors.modalCommentElem).html(commentInput.value);
        }
        $(selectors.modalCommentContainer).show();
    } else {
        $(selectors.modalCommentContainer).hide();
    }
    
    const modalElem: HTMLDivElement = document.querySelector(selectors.modalContainer);
    modalElem.dataset.formId = form.id;
}
