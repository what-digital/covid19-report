import NameValuePair = JQuery.NameValuePair;
import {initEditableForm} from 'global/ts/polling/edit-last-answer';
import Noty from 'noty';
import * as Sentry from '@sentry/browser';


export function submitFormUsingAjax(formElem: HTMLFormElement) {
    const fieldsArray: NameValuePair[] = $(formElem).serializeArray();
    let jsonData = {};
    for (const nameValue of fieldsArray) {
        jsonData[nameValue.name] = nameValue.value;
    }
    const csrfToken = jsonData['csrfmiddlewaretoken'];
    delete jsonData['csrfmiddlewaretoken'];
    console.log(jsonData);
    $.ajax({
        type: 'PATCH',
        url: formElem.action,
        data: jsonData,
        dataType: 'json',
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', csrfToken);
        },
        success: (response) => {
            makeFormAsEditable(formElem);
            notify("Saved");
        },
        error: (request, timeout, _) => {
            let errorElem = formElem.querySelector('.error');
            if (request.responseJSON) {
                const responseJson = request.responseJSON;
                let errorList = [];
                for (const errorName in responseJson) {
                    if (responseJson.hasOwnProperty(errorName)) {
                        errorList.push(responseJson[errorName])
                    }
                }
                Sentry.captureMessage(errorList.toString());
                notify(errorList.toString(), 'error');
            } else {
                Sentry.captureMessage(request.responseJSON);
                errorElem.textContent = "An error occurred, please try again later or contact support";
            }
        }
    });
}


function notify(message: string, type: Noty.Type = 'success') {
    new Noty({
        text: message,
        type: type,
        theme: 'bootstrap-v4',
        timeout: 3000,
    }).show();
}


function makeFormAsEditable(formElem: HTMLFormElement) {
    formElem.dataset.isFilled = String(true);
    initEditableForm(formElem);
}
