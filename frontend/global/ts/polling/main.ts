import {initEditableActions} from 'global/ts/polling/edit-last-answer';
import {showHideComments} from 'global/ts/polling/comment-checkbox';
import {confirmModalInit} from 'global/ts/polling/confirmation-modal';
import {initNavOrgSelect} from 'global/ts/polling/nav';


export function initPooling() {
    showHideComments();
    confirmModalInit();
    initEditableActions();
    initNavOrgSelect();
}
