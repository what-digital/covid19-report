export function showHideComments() {

    $(document).on("click", ".comment-checkbox", function () {
        const checkboxElem = this;
        if ($(checkboxElem).prop("checked") == true) {
            showCommentBox(checkboxElem);
        } else if ($(checkboxElem).prop("checked") == false) {
            $("#comment-input-container-" + $(checkboxElem).attr('id')).hide();
    
            $("#comment-input-container-" + $(checkboxElem).attr('id')).find("input[type='text']:first").val('');
        }
    });
}


export function showCommentBox(checkboxElem) {
    $("#comment-input-container-" + $(checkboxElem).attr('id')).show();
}
