import {initPooling} from 'global/ts/polling/main';


export function main(): any {
    window.addEventListener('load', (event) => {
        initPooling();
    });
}


main();
