How-to guides
=============

Work in and with the Dockerised environment
----------------------------------------------

Almost any command can be executed inside the Dockerised environment. One way is to precede the command with:

..  code-block:: bash

    docker-compose run web

followed by the command. For example, to run Django migrations

..  code-block:: bash

    docker-compose run web python manage.py migrate

If you need to run several commands it may be more convenient to open a bash [or fish] shell:

..  code-block:: bash

    docker-compose run web bash [or fish]

or a Django shell:

..  code-block:: bash

    docker-compose run web python manage.py shell


Update ``requirements.txt``
---------------------------

The project uses the Python dependencies listed in ``requirements.txt``. This is a fully-pinned list of all
dependencies. The list is derived from the project's ``requirements.in``.

The build process uses ``requirements.txt`` if it's present, *not* ``requirements.in``. To update
``requirements.txt``:

..  code-block:: bash

    docker-compose run pip-reqs compile

To rebuild the containers with the new requirements:

..  code-block:: bash

    docker-compose build

(The actual build process will also execute ``pip-reqs resolve`` (which creates a ``requirements.urls`` file)
followed by ``pip install --no-index --no-deps --requirement requirements.urls`` (which uses the
``requirements.urls`` file).

See `How to pin all of your project’s Python dependencies
<https://docs.divio.com/en/latest/how-to/resources-pin-dependencies.html>`_ in the Divio developer handbook for
more information about how this works.


Interact with the local and cloud database
--------------------------------------------------

See `How to interact with your project’s database
<https://docs.divio.com/en/latest/how-to/interact-database.html>`_ in the Divio developer handbook.


How to build the documentation
------------------------------

The documentation is written in `Restructured Text
<https://docutils.readthedocs.io/en/sphinx-docs/user/rst/quickstart.html>`_, built with `Sphinx
<https://www.sphinx-doc.org/en/master/>`_ and hosted at `Read the Docs <https://readthedocs.com>`_.

The documentation can be built locally:

..  code-block:: bash

    cd docs  # navigate to the documentation directory

    # use the supplied Makefile to set up a virtual environment for building the documentation
    # - this only needs to be done once
    make install

    make run  # builds and serve the documentation

The documentation is served at http://localhost:9090/.
