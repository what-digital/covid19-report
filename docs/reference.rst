Reference
=========

Key components
-------------------------------------------------------------------------------

* Python 3.6
* Django 2.2
* Webpack 4
* TypeScript 3


Backend (Django) code
---------------------

The root ``backend`` module is reserved for non-project related applications, eg ``auth`` or ``articles``.

Project-specific applications should live in a project module of their own, for example ``backend.dectris``.

Similarly, the ``backend/templates`` directory is only for global templates. For anything, else use app-specific
templates (i.e. do things the standard Django way).


Frontend code
---------------------

For any questions, please refer to Victor Yunenko (victor.yunenko@what.digital)

Project-specific scss styles can be in ``global``; however, JS logic must not be.

When you need to add a script for a new page/block add a new ``entry`` in ``webpack.config.js`` and a page/block
submodule.

For global scripts and styles use the ``global`` entry, and add the respective HTML code in your template.

If you need to add a global variable to JS, add it to the ``backend/templates/default.html#DJANGO`` const and
extend ``frontend/global/ts/django.ts#DJANGO``.

If you need to add a static file, e.g. on URL ``https://localhost/static/img/icon.png``, add it to ``frontend/`` -
everything in there will be accessible under the ``/static/`` URL path.

If you need to add a new font to CSS, use the global path as you normally would, e.g. ``url('~pages/homepage/fonts/frutiger.woff')``.

If you need to add images or other assets, add them under the respective module, e.g. ``global``, ``vendor``, ``pages/homepage``.

We use 24 columns in our Bootstrap 4 configuration.


.. _contributors:

Contributors
-------------

* Victor Yunenko victor.yunenko@what.digital
* Jonathan Stoppani jonathan.stoppani@divio.com
* Anand Patel anand@what.digital
* Elliott White elliott@what.digital
* Mario Colombo mario@what.digital
* Marcus Kuhn marcus@what.digital
* Daniele Procida daniele.procida@divio.com
