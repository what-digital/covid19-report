Explanation
============

Key functionality
-----------------

Administrators can:

* can create organisations and invite users to them
* manage groups that to which different organisations are assigned
* manage questions, that are assigned to groups; groups determine which questions apply to each organisation
* export all answers provided by users for purposes of data analysis

Users can:

* see the questions for their organisations
* enter and edit (until a deadline) their answers

The system will notify users when answers are overdue.

In development
--------------

The system will provide real-time reporting of the data, in a dashboard accessible only to authorised users.
