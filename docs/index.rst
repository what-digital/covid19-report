.. COVID-19 Report documentation master file, created by
   sphinx-quickstart on Fri Mar 20 18:55:51 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

CoReport - a COVID-19 reporting application
===========================================

CoReport is an `open-source <https://gitlab.com/what-digital/covid19-report/-/blob/master/LICENSE>`_ tool that
enables an organisation to collect structured status reports from other organisations at set intervals.


The basic concept
-----------------

An **organisation** that must make plans and allocate resources to deal with COVID-19, such a health board or city authority,
needs regular updates of **information**. This can be quantitative information concerning things like available hospital beds
and other equipment, staff, confirmed cases and so on, or qualitative information, such as notice of key decisions, situation
assessments, etc.

This information needs to come from a wide variety of disparate **sources**, from hospitals and police services to community
organisations and volunteers. At the same time, it needs to be consistently structured so that it can be processed quickly and
accurately.

CoReport does this by allowing the organisation that deploys it to define the **questions** that need to be answered (including
the appropriate frequency and deadlines for questions) and the different sources information needs to come from.

For example (a real set-up would include many more than this):

* *hospitals* may need to report daily on infection and testing numbers, the number of beds and ventilators available or
  supplies urgently required
* *police, military and fire services* may need to report thrice a week on the availability of personnel
* *community organisations* may need to report daily on matters of local concern

Reporting users of the application are prompted for the information, and reminded of expired deadlines.

The data thus captured can be exported in a consistent way in a variety of formats for further processing as required.


About development
-----------------

CoReport is currently being deployed by the Swiss canton of Baselland, on hosting sponsored by `Divio
<https://www.divio.com>`_. Divio is also donating further hosting of the tool for other organizations.

The development of CoReport was initiated by `what. <https://what.digital>`_ in collaboration with Divio (see our
:ref:`contributors`). The first iteration of the tool was developed for the Swiss canton of Baselland.


`Project source code <https://gitlab.com/what-digital/covid19-report/>`_


Contact us
----------

For assistance with deployment, please contact `Divio <mailto:letstalk@divio.com>`_.

For all other queries, please contact contact@coreport.ch.


.. toctree::
   :maxdepth: 2
   :caption: Contents

   get-started
   how-to
   reference
   background
