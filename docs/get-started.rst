Get started
===========

Set up a local instance for development
---------------------------------------

..  note::

    CoReport is a project in development. The project and this documentation are updated regularly, and these notes
    may have changed since the last time you read them.


Clone the codebase:

..  code-block:: bash

    git clone git@gitlab.com:what-digital/covid19-report.git

The project requires that you have Docker installed; you'll need:

* Macintosh users: `Docker for Mac <https://docs.docker.com/docker-for-mac/>`_
* Windows users: `Docker for Windows <https://docs.docker.com/docker-for-windows/>`_
* Linux users: `Docker CE <https://docs.docker.com/install/#server>`_ and `Docker Compose
  <https://docs.docker.com/compose/install/>`_

``cd`` into the ``covid19-report`` directory and build the application:

..  code-block:: bash

    docker-compose build

This will create images based on the services described in the ``docker-compose.yml`` file, including the
``web`` service, The Celery services, the Postgres database and any others.

Migrate the database:

..  code-block:: bash

    docker-compose run web python manage.py migrate

This runs the Django ``manage.py migrate`` inside a ``web`` container.

Launch the application:

..  code-block:: bash

    docker-compose up

It will take a few moments to start up all the containers, and then you will be able to reach the application at
http://localhost:8000 (this is configured in ``docker-compose.yml``). You should see an **unstyled** login page.

In order to generate the styling for the site, along with all its other frontend components, they will need to be
built manually. This requires Yarn, a Node package, to be installed. (It is beyond the scope of this documentation
to describe that process.)

In a new terminal session in the project directory, run:

..  code-block:: bash

    yarn install --pure-lockfile

This installs the components that the project's frontend requires. Then:

..  code-block:: bash

    yarn start

This launches the frontend; the frontend assets will be compiled and the served on port 8090. Yarn watches the
frontend source and will re-compile the assets on any change.

When you refresh the login page, the styling should now appear.


Prepare the project for cloud deployment
--------------------------------------------

The CoReport project is ready for deployment on the Divio cloud management platform, and the project already
contains the components required for this. All your repository requires in order to test in a cloud deployment is
to associate it with a Divio cloud project.

Using the Divio Control Panel (you will need an account, it's free), `create a new project
<https://control.divio.com/control/project/create>`_.

Select the defaults:

* *Platform*: ``Python``
* *Type*: ``Django``

You can use Divio's Git server (also the default).

Hit **Skip** in the *Subscription* view.

After a few moments, you'll be presented with its dashboard, which will look something like this:

.. image:: /images/project-undeployed.png
   :alt: 'New project in the Dashboard'

Install the `Divio CLI <https://github.com/divio/divio-cli>`_: ``pip install divio-cli``.

Run:

..  code-block:: bash

    divio login

This will fetch a token from https://control.divio.com/account/desktop-app/access-token/.

Upload your public key to https://control.divio.com/account/ssh-keys/

Get your project's *slug* from the Control Panel:

.. image:: /images/project-slug.png
   :alt: 'Project slug'
   :width: 400px

Or you can see it by listing your projects:

..  code-block:: bash

    divio project list

Set up the project:

..  code-block:: bash

    divio project setup <project slug>

Once the ``divio project setup`` command has finished pulling down the repository, you will find a file in the
newly-created directory:: ``.aldryn``. The file contains the slug and id of the cloud project, and is what the
Divio CLI uses to associate the local project with its counterpart on the cloud.

Move this file into the CoReport ``covid19-report`` project.

We need to do a similar thing with Git. In the local Divio project, find its Git URL:

..  code-block:: bash

    git remote show origin

In the ``covid19-report`` project, add a new remote called ``divio``, using this URL, for example:

..  code-block:: bash

    git remote add divio git@git.divio.com:my-divio-coreport-project.git

Your ``covid19-report`` project is now associated with the Divio cloud project you just created. You can dispose
of the local project that was created by the ``divio project setup`` command; it's no longer needed.

You can now interact with both the ``origin`` remote (for example, to pull new updates) and push your changes to
the ``divio`` remote (to test it on the cloud).


Working with the project
------------------------

Basic workflow:

..  code-block:: bash

    git add <files>  # to stage changes for commit
    git commit  # to commit changes
    git push divio  # push changes to your cloud project
    divio project deploy  # deploy the cloud Test server


Also useful:

..  code-block:: bash

    divio project pull [or push] db  # copy the database to/from the cloud project

You can access the Test and Live sites of your cloud project via the Control Panel. See the Divio developer
handbook for `a quick guide to using the Divio CLI <https://docs.divio.com/en/latest/how-to/local-cli.html>`_.

Whenever you pull or make changes that will require rebuilding the local project (for example, changing
``requirements.in``) re-run:

..  code-block:: bash

    docker-compose build
